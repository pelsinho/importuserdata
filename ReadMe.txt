## Title

  Import User Data

## Technical Environment Rules

  Please just implement the API, and skip UI. It’s also fine to skip the authentication.
  The purpose is not to build a solid API but for you to demonstrate your coding skills and engineering practices.
  The application has to follow OOP approach, so no specific framework knowledge is needed.

## Goal

  Build an Import User Data API.
  Create an application for importing users data from a CSV file.

  Requirements

  The file can contain an arbitrary number of rows. Each row contains such fields in the following order:

    - email
    - lastname
    - firstname
    - fiscal code
    - description
    - last access date

  Assumptions :

    - There cannot be more users with the same email in the DB
    - All fields (with the exception of email) can be null or empty
    - Any file is the new master

  Scenarios :

    - Given a new file to import, when I have imported the file, then the db state has to be updated
    - Given an existing user, when the new imported file contains the same email, then the existing user data has to be updated
    - Given a new file to import, when I upload a *.txt file, then I should receive an error
    - Given a new file to import, when I upload a file that exceeds the limit (in MB), then I should receive an error

  The expectations are:

    - Modelling needed entities
    - Handling errors
    - Saving the file everywhere you prefer (Keep in mind the storage provider could change)
    - Highlighting complexities you have to cover
    - Being able to import a file containing 10k rows

  Writing tests!

## Tooling

  1. Please install IntelliJ or Eclipse Lombok plugin, to avoid possible compilation problems
  2. Java 11
  3. Maven
  4. Application is configure to use 8081 http port. If you need please switch on application.properties (server.port), or adding a Env_Variable like : SERVER_HTTP_PORT = 8081

## Installation

  1. Add this project in any computer directory

## Compilation

  1. Go to project root folder
  2. Compile the project using : mvn clean install. You need to install maven in your computer.

## Start

  1. Go to project root folder
  2. Start the application using : mvn spring-boot:run or ./mvnw spring-boot:run

# First Steps

  1. After start the application please go to SWAGGER UI Home Page (http://localhost:8081/swagger-ui.html) and read application rules, and information.
  2. You are also going to be able to test all application features from this Home Page.
  3. For a better user experience please take some minutes to read our instructions before start using it.

## Tech Stack

  1. Spring Framework

      - Spring Boot, JPA, Web, DevTools, etc.
      - Please check POM for more information

  2. Lombok Plugin :

      - Reduce boiler plate code

  3. CheckStyle :

      - Code style pattern plugin (checkstyle.xml)

  4. Database : H2

      - Url : http://localhost:8081/h2
      - User : sa
      - Password : sa

  5. Jacoco :

      - Generate Test Reports to be used in Sonar or Jenkins.

  6. Maven Profiles : Dev and Prd

      - You can change default maven profile on pom.xml file, using <activeByDefault>true</activeByDefault>
      - or when you start application using -P flag in mvn command. Example : mvn install -P Prd

  7. Spring Profiles : Dev and Prd

      - You can switch on application.properties (spring.profiles.active), or adding a Env_Variable like : SPRING_PROFILE = PRD

  8. SQL Log :

      - Set as DEBUG for Spring Dev Profile
      - Set as INFO for Spring Prd Profile
      - You can switch on application.properties (logging.level.org.hibernate.SQL), or adding a Env_Variable like : SQL_LOG_LEVEL

  9. Spring Live Reload :

      - I´ve enabled Spring Live Reload, so when you change something in your code, you don't need to restart it to enable those . changes.
      - Just recompile your project and test your change right away :
          . Option 1 : Build -> Recompile
          . Option 2 : Shift + Command + 9 on Mac


## Spring Admin Monitor Server

      This application is ready to be used with my Spring Admin Monitor Server project.
      If you want to use/test this functionality please :

        First

            Uncomment this line in application.properties file

            - #spring.boot.admin.client.url=http://localhost:8090

        Second :

            Download and start my Spring Admin Monitor Server using this link :

            - https://gitlab.com/pelsinho/spring-boot-admin-server.git

            - All necessary information will be in ReadMe.txt project file.

## Tests

  1. Unit Tests :

    - Start Unit Tests using : mvn clean test
    - Folder : Src -> Main -> Test -> Java

  2. Functional Tests :

    - First Start Interview Application
    - Start Functional Tests using : mvn -Dgroups='functional' failsafe:integration-test@api-tests failsafe:verify
    - Folder : Src -> Main -> Test -> Java -> functionaltests

    Please let me know if there is a problem running this command.

  3. Tests Support Classes :

    - Folder : Src -> Main -> Test -> Java -> testsupport


## GIT Lab

  If you want to login in to validate the project and the pipepline in GITLab you can use this login / password : 

    1 - User : ngomesgitlab
    2 - Password : ngomesgitlab

  I´ve setup a CI Pipeline to validate & control our Continuous Integration process.

  Phases :

    1 - Validate CheckStyle
    2 - Code Compilation
    3 -  Run Tests
      3.1 - JUnit Tests
      3.2 - Functional Tests
    4 - Generate Application Package
    5 - Deploy Application (TODO)

## Design Patterns

  1. I like to use Chain of Responsibility Design Pattern during validation process to de-couple one validation from another and to avoid create a LOT of if´s.
