package functionaltests.tests;

import functionaltests.connectors.UserImportDataHttpActions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;

import java.io.IOException;

import static functionaltests.common.enums.InvalidMultiPartFiles.*;
import static testsupport.examples.AbstractExamples.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@ExtendWith(SpringExtension.class)
class Import_Invalid_MultiPartFiles_FT extends AbstractExamples {

    /**
     * Clean DB
     */
    @BeforeEach
    public void initEach() throws IOException {
        UserImportDataHttpActions.deleteAllUserDataDB(200);
    }

    /**
     * This tests are going to import a lot of Invalid User Data Files Scenarios
     * Being Tested/Asserted in another class
     */
    @SuppressWarnings("squid:S2699")
    @Test
    void should_Receive_400_When_Import_InValid_User_Data_File_Extension_Invalid() {

        UserImportDataHttpActions.importUserDataWithError(
                INVALID_IMPORT_USER_DATA_EXTENSION_TXT.getName(),
                400);

    }

    /**
     * Being Tested/Asserted in another class
     */
    @SuppressWarnings("squid:S2699")
    @Test
    void should_Receive_400_When_Import_InValid_User_Data_File_Two_Repeated_Emails_Invalid() {

        UserImportDataHttpActions.importUserDataWithError(
                INVALID_IMPORT_USER_DATA_TWO_REPEATED_EMAILS.getName(),
                400);

    }

    /**
     * Being Tested/Asserted in another class
     */
    @SuppressWarnings("squid:S2699")
    @Test
    void should_Receive_400_When_Import_InValid_User_Data_File_Invalid_Description() {

        UserImportDataHttpActions.importUserDataWithError(
                INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_DESCRIPTION.getName(),
                400);

    }

    /**
     * Being Tested/Asserted in another class
     */
    @SuppressWarnings("squid:S2699")
    @Test
    void should_Receive_400_When_Import_InValid_User_Data_File_Invalid_Email() {

        UserImportDataHttpActions.importUserDataWithError(
                INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_EMAIL.getName(),
                400);

    }

    /**
     * Being Tested/Asserted in another class
     */
    @SuppressWarnings("squid:S2699")
    @Test
    void should_Receive_400_When_Import_InValid_User_Data_File_Invalid_First_Name() {

        UserImportDataHttpActions.importUserDataWithError(
                INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_FIRST_NAME.getName(),
                400);

    }

    /**
     * Being Tested/Asserted in another class
     */
    @SuppressWarnings("squid:S2699")
    @Test
    void should_Receive_400_When_Import_InValid_User_Data_File_Invalid_Fiscal_Code() {

        UserImportDataHttpActions.importUserDataWithError(
                INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_FISCAL_CODE.getName(),
                400);

    }

    /**
     * Being Tested/Asserted in another class
     */
    @SuppressWarnings("squid:S2699")
    @Test
    void should_Receive_400_When_Import_InValid_User_Data_File_Invalid_Last_Access_Data() {

        UserImportDataHttpActions.importUserDataWithError(
                INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_LAST_ACCESS_DATA.getName(),
                400);

    }

    /**
     * Being Tested/Asserted in another class
     */
    @SuppressWarnings("squid:S2699")
    @Test
    void should_Receive_400_When_Import_InValid_User_Data_File_Invalid_Last_Name() {

        UserImportDataHttpActions.importUserDataWithError(
                INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_NO_LAST_NAME.getName(),
                400);

    }

    /**
     * Being Tested/Asserted in another class
     */
    @SuppressWarnings("squid:S2699")
    @Test
    void should_Receive_400_When_Import_InValid_User_Data_File_Invalid_No_Email() {

        UserImportDataHttpActions.importUserDataWithError(
                INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_NO_EMAIL.getName(),
                400);

    }


}
