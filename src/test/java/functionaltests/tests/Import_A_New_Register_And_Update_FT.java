package functionaltests.tests;

import functionaltests.connectors.UserImportDataHttpActions;
import importing.user.data.data.dto.UserDataDTO;
import importing.user.data.data.entities.UserDataDB;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.validation.ValidateObject;

import java.io.IOException;
import java.util.List;

import static functionaltests.common.enums.ValidMultiPartFiles.VALID_IMPORT_USER_DATA_COMPLETE_TWO_EMAILS_V0;
import static functionaltests.common.enums.ValidMultiPartFiles.VALID_IMPORT_USER_DATA_COMPLETE_TWO_EMAILS_V1;
import static functionaltests.common.enums.VersionFiles.V0;
import static functionaltests.common.enums.VersionFiles.V1;
import static testsupport.examples.AbstractExamples.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@ExtendWith(SpringExtension.class)
class Import_A_New_Register_And_Update_FT {

    /**
     * We are going to create
     */
    @Test
    void import_InValid_User_Data_File_Extension_Invalid() throws IOException {

        /**
         * DELETE DATABASE TO MAKE SURE THERE IS NO DATA THERE
         */

        UserImportDataHttpActions.deleteAllUserDataDB(200);

        /**
         * CREATE User Data Version 0;
         * And Validate There is Only V0
         */

        List<UserDataDTO> userDataDTOListV0 = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_COMPLETE_TWO_EMAILS_V0.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOListV0, true, true, true, true, true, true);

        ValidateObject.validateUserDataDTOHasCorrectTestVersion(userDataDTOListV0, V0);

        /**
         * Get All from Database and validate version
         * And Validate There is Only V0
         */

        List<UserDataDB> allUserDataDBListV0 = UserImportDataHttpActions.getAllUserDataDB(200);

        ValidateObject.validateUserDataDBHasCorrectTestVersion(allUserDataDBListV0, V0);

        /**
         * Update User Data Version 1;
         * And Validate There is Only V1
         */

        List<UserDataDTO> userDataDTOListV1 = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_COMPLETE_TWO_EMAILS_V1.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOListV1, true, true, true, true, true, true);

        ValidateObject.validateUserDataDTOHasCorrectTestVersion(userDataDTOListV1, V1);

        /**
         * Get All from Database and validate version
         * And Validate There is Only V1
         */

        List<UserDataDB> allUserDataDBListV1 = UserImportDataHttpActions.getAllUserDataDB(200);

        ValidateObject.validateUserDataDBHasCorrectTestVersion(allUserDataDBListV1, V1);

    }

}
