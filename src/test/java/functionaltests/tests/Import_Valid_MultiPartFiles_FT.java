package functionaltests.tests;

import functionaltests.connectors.UserImportDataHttpActions;
import importing.user.data.data.dto.UserDataDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;
import testsupport.validation.ValidateObject;

import java.io.IOException;
import java.util.List;

import static functionaltests.common.enums.ValidMultiPartFiles.*;
import static testsupport.examples.AbstractExamples.TAG_FUNCTIONAL;

@Tag(TAG_FUNCTIONAL)
@ExtendWith(SpringExtension.class)
class Import_Valid_MultiPartFiles_FT extends AbstractExamples {

    /**
     * Clean DB
     */
    @BeforeAll
    public static void init() throws IOException {
        UserImportDataHttpActions.deleteAllUserDataDB(200);
    }

    /**
     * This tests are going to import a lot of Valid User Data Files Scenarios
     */
    @Test
    void import_Valid_User_Data_File_Full_Object_With_One_Email() {

        List<UserDataDTO> userDataDTOList = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_COMPLETE_ONE_EMAIL.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOList, true, true, true, true, true, true);
    }

    @Test
    void import_Valid_User_Data_File_Full_Object_With_Two_Email_V0() {

        List<UserDataDTO> userDataDTOList = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_COMPLETE_TWO_EMAILS_V0.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOList, true, true, true, true, true, true);
    }

    @Test
    void import_Valid_User_Data_File_Full_Object_With_Two_Email_V1() {

        List<UserDataDTO> userDataDTOList = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_COMPLETE_TWO_EMAILS_V1.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOList, true, true, true, true, true, true);
    }

    @Test
    void import_Valid_User_Data_File_Full_Object_Withouth_Description() {

        List<UserDataDTO> userDataDTOList = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_DESCRIPTION.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOList, true, true, true, true, false, true);
    }

    @Test
    void import_Valid_User_Data_File_Full_Object_Withouth_First_Name() {

        List<UserDataDTO> userDataDTOList = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_FIRST_NAME.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOList, true, true, false, true, true, true);
    }

    @Test
    void import_Valid_User_Data_File_Full_Object_Withouth_Last_Name() {

        List<UserDataDTO> userDataDTOList = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_LAST_NAME.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOList, true, false, true, true, true, true);
    }

    @Test
    void import_Valid_User_Data_File_Full_Object_Withouth_Last_Access_Data() {

        List<UserDataDTO> userDataDTOList = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_LAST_ACCESS_DATA.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOList, true, true, true, true, true, false);
    }

    @Test
    void import_Valid_User_Data_File_Full_Object_Withouth_Fiscal_Code() {

        List<UserDataDTO> userDataDTOList = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_FISCAL_CODE.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOList, true, true, true, false, true, true);
    }

    @Test
    void import_Valid_User_Data_File_Only_Email() {

        List<UserDataDTO> userDataDTOList = UserImportDataHttpActions.importUserData(
                VALID_IMPORT_USER_DATA_COMPLETE_ONLY_EMAIL.getName(),
                200);

        ValidateObject.validateUserDataDTO(
                userDataDTOList, true, false, false, false, false, false);
    }

}
