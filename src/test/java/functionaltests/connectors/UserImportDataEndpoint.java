package functionaltests.connectors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserImportDataEndpoint {

    MAIN_URL("http://localhost:8081"),

    USER_DATA("/userdata"),

    USER_DATA_DEV("/usertestdata"),

    IMPORT_USER_DATA(MAIN_URL.getEndpoint() + USER_DATA.getEndpoint()),

    GET_ALL_USER_DATA(MAIN_URL.getEndpoint() + USER_DATA_DEV.getEndpoint()),

    DELETE_ALL_USER_DATA(MAIN_URL.getEndpoint() + USER_DATA_DEV.getEndpoint());

    private final String endpoint;

}
