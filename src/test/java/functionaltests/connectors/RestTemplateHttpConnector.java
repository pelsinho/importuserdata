package functionaltests.connectors;


import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.data.dto.UserDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

@Slf4j
public class RestTemplateHttpConnector {


    public static ResponseEntity<List<UserDataDTO>> httpPostWithMultiPart(String url, String pathFileName) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", getFile(pathFileName));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();

        ParameterizedTypeReference<List<UserDataDTO>> responseType =
                new ParameterizedTypeReference<List<UserDataDTO>>() {};

        return restTemplate.exchange(url, HttpMethod.POST, requestEntity, responseType);

    }


    public static void httpPostWithMultiPartExpectError(String url, String pathFileName) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", getFile(pathFileName));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();

        try {
            restTemplate.postForEntity(url, requestEntity, String.class).getStatusCode();
            Assert.fail();
        } catch (Exception e) {
            return;
        }

    }




    private static FileSystemResource getFile(String pathFileName) {
        final File file = new File(pathFileName);
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.negate().test(data));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            Assert.fail("Unable to Read File : " + pathFileName);
        }

        return new FileSystemResource(file);

    }


    public static void validateStatus(int expectedStatus, ResponseEntity responseEntity) {
        assertEquals(expectedStatus, responseEntity.getStatusCode().value());
    }


}
