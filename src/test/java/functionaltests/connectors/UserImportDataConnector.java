package functionaltests.connectors;

import importing.user.data.data.dto.UserDataDTO;
import org.apache.http.HttpResponse;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;

import static functionaltests.connectors.UserImportDataEndpoint.*;

public class UserImportDataConnector {

    public static ResponseEntity<List<UserDataDTO>> importUserData(
            String multipartFileWithPath) {

        String url = IMPORT_USER_DATA.getEndpoint();
        return RestTemplateHttpConnector.httpPostWithMultiPart(url, multipartFileWithPath);

    }

    public static void importUserDataExpectError(
            String multipartFileWithPath) {

        String url = IMPORT_USER_DATA.getEndpoint();
        RestTemplateHttpConnector.httpPostWithMultiPartExpectError(url, multipartFileWithPath);

    }


    public static HttpResponse getAllUserData() throws IOException {

        String url = GET_ALL_USER_DATA.getEndpoint();
        return new ApacheHttpConnector().httpGet(url);

    }

    public static HttpResponse deleteAllUserData() throws IOException {

        String url = DELETE_ALL_USER_DATA.getEndpoint();
        return new ApacheHttpConnector().httpDelete(url);

    }


}
