package functionaltests.connectors;

import com.fasterxml.jackson.core.type.TypeReference;
import importing.user.data.data.dto.UserDataDTO;
import importing.user.data.data.entities.UserDataDB;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.junit.Assert;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class UserImportDataHttpActions {

    private static final List<Integer> EXPECT_ERROR_STATUS_LIST = List.of(400, 402, 404, 500);


    /**
     * IMPORT USER DATA
     * @return
     */

    public static List<UserDataDTO> importUserData(
            String multipartFileWithPath,
            Integer expectedStatus){

        ResponseEntity<List<UserDataDTO>> responseEntity = UserImportDataConnector.importUserData(multipartFileWithPath);

        RestTemplateHttpConnector.validateStatus(expectedStatus, responseEntity);

        return responseEntity.getBody();

    }


    public static void importUserDataWithError(
            String multipartFileWithPath,
            Integer expectedStatus){

        //Only To Check Errors
        Assert.assertTrue(EXPECT_ERROR_STATUS_LIST.contains(expectedStatus));
        UserImportDataConnector.importUserDataExpectError(multipartFileWithPath);

    }



    /**
     * GET ALL DATA
     * Not Works in Production
     */


    public static List<UserDataDB> getAllUserDataDB(Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = UserImportDataConnector.getAllUserData();

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

        if (EXPECT_ERROR_STATUS_LIST.contains(expectedStatus)) return new ArrayList<>();

        return ApacheHttpConnector.readResponse(httpResponse, new TypeReference<>() {});

    }


    /**
     * DELETE ALL DATA
     * Not Works in Production
     */


    public static void deleteAllUserDataDB(Integer expectedStatus) throws IOException {

        HttpResponse httpResponse = UserImportDataConnector.deleteAllUserData();

        ApacheHttpConnector.validateStatus(expectedStatus, httpResponse);

    }



}
