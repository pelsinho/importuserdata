package functionaltests.common.constants;

public class FTTestEnvironmentConstants {

    public static final String VALID_MULTI_PART_FILES_SAMPLES_PATH = "./src/test/resources/multipartfiles/valid/";
    public static final String INVALID_MULTI_PART_FILES_SAMPLES_PATH = "./src/test/resources/multipartfiles/invalid/";

}
