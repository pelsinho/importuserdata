package functionaltests.common.enums;


import static functionaltests.common.constants.FTTestEnvironmentConstants.VALID_MULTI_PART_FILES_SAMPLES_PATH;

public enum ValidMultiPartFiles {

    VALID_IMPORT_USER_DATA_COMPLETE_ONE_EMAIL(VALID_MULTI_PART_FILES_SAMPLES_PATH + "validImportUserData_Complete_One_Email.csv"),
    VALID_IMPORT_USER_DATA_COMPLETE_ONLY_EMAIL(VALID_MULTI_PART_FILES_SAMPLES_PATH + "validImportUserData_Complete_Only_Email.csv"),
    VALID_IMPORT_USER_DATA_COMPLETE_TWO_EMAILS_V0(VALID_MULTI_PART_FILES_SAMPLES_PATH + "validImportUserData_Complete_Two_Emails_Version_0.csv"),
    VALID_IMPORT_USER_DATA_COMPLETE_TWO_EMAILS_V1(VALID_MULTI_PART_FILES_SAMPLES_PATH + "validImportUserData_Complete_Two_Emails_Version_1.csv"),
    VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_DESCRIPTION(VALID_MULTI_PART_FILES_SAMPLES_PATH + "validImportUserData_Two_Emails_No_Description.csv"),
    VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_FIRST_NAME(VALID_MULTI_PART_FILES_SAMPLES_PATH + "validImportUserData_Two_Emails_No_First_Name.csv"),
    VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_FISCAL_CODE(VALID_MULTI_PART_FILES_SAMPLES_PATH + "validImportUserData_Two_Emails_No_Fiscal_Code.csv"),
    VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_LAST_ACCESS_DATA(VALID_MULTI_PART_FILES_SAMPLES_PATH + "validImportUserData_Two_Emails_No_Last_Access_Data.csv"),
    VALID_IMPORT_USER_DATA_TWO_EMAILS_NO_LAST_NAME(VALID_MULTI_PART_FILES_SAMPLES_PATH + "validImportUserData_Two_Emails_No_Last_Name.csv");

    private String name;

    ValidMultiPartFiles(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static ValidMultiPartFiles lookup(String name) {
        return Enum.valueOf(ValidMultiPartFiles.class, name.toUpperCase());
    }

}
