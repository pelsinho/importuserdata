package functionaltests.common.enums;


import static functionaltests.common.constants.FTTestEnvironmentConstants.INVALID_MULTI_PART_FILES_SAMPLES_PATH;

public enum InvalidMultiPartFiles {

    INVALID_IMPORT_USER_DATA_EXTENSION_TXT(INVALID_MULTI_PART_FILES_SAMPLES_PATH + "invalidImportUserData_Invalid_Extension_Txt.txt"),
    INVALID_IMPORT_USER_DATA_TWO_REPEATED_EMAILS(INVALID_MULTI_PART_FILES_SAMPLES_PATH + "invalidImportUserData_Invalid_Two_Repeated_Emails.csv"),
    INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_DESCRIPTION(INVALID_MULTI_PART_FILES_SAMPLES_PATH + "invalidImportUserData_Two_Emails_Invalid_Description.csv"),
    INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_EMAIL(INVALID_MULTI_PART_FILES_SAMPLES_PATH + "invalidImportUserData_Two_Emails_Invalid_Email.csv"),
    INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_FIRST_NAME(INVALID_MULTI_PART_FILES_SAMPLES_PATH + "invalidImportUserData_Two_Emails_Invalid_First_Name.csv"),
    INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_FISCAL_CODE(INVALID_MULTI_PART_FILES_SAMPLES_PATH + "invalidImportUserData_Two_Emails_Invalid_Fiscal_Code.csv"),
    INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_LAST_ACCESS_DATA(INVALID_MULTI_PART_FILES_SAMPLES_PATH + "invalidImportUserData_Two_Emails_Invalid_Last_Access_Data.csv"),
    INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_NO_LAST_NAME(INVALID_MULTI_PART_FILES_SAMPLES_PATH + "invalidImportUserData_Two_Emails_Invalid_Last_Name.csv"),
    INVALID_IMPORT_USER_DATA_TWO_EMAILS_INVALID_NO_EMAIL(INVALID_MULTI_PART_FILES_SAMPLES_PATH + "invalidImportUserData_Two_Emails_No_Email.csv");

    private String name;

    InvalidMultiPartFiles(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static InvalidMultiPartFiles lookup(String name) {
        return Enum.valueOf(InvalidMultiPartFiles.class, name.toUpperCase());
    }

}
