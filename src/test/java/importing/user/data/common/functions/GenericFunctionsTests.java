package importing.user.data.common.functions;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

@ExtendWith(SpringExtension.class)
class GenericFunctionsTests extends AbstractExamples {


    /**
     * getMapValueAsStringBy
     */

    @Test
    void should_GetMapValueAsStringBy_From_Null() {

        String mapToString = GenericFunctions.getMapValueAsStringBy.apply(null, ",");
        Assert.assertNotNull(mapToString);
        Assert.assertTrue(mapToString.isEmpty());

    }


    @Test
    void should_GetMapValueAsStringBy_From_Empty() {

        String mapToString = GenericFunctions.getMapValueAsStringBy.apply(new HashMap<>(), ",");
        Assert.assertNotNull(mapToString);
        Assert.assertTrue(mapToString.isEmpty());

    }


    @Test
    void should_GetMapValueAsStringBy_From_Valid_Map() {

        Map<String, String> stringStringMap = Map.of("1", "Try", "2", "Check", "3", "Again");

        String mapToString = GenericFunctions.getMapValueAsStringBy.apply(stringStringMap, ",");

        Assert.assertTrue(mapToString.contains("Try"));
        Assert.assertTrue(mapToString.contains("Check"));
        Assert.assertTrue(mapToString.contains("Again"));

    }


    /**
     * getSlotDateTimeFormatter
     */

    @Test
    void should_Throw_NullPointerException_GetSlotDateTimeFormatter_From_Null() {
        Assert.assertThrows(NullPointerException.class, () -> GenericFunctions.getLocalDateFormatter.apply(null));
    }

    @Test
    void should_Throw_DateTimeParseException_GetSlotDateTimeFormatter_From_Empty() {
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getLocalDateFormatter.apply(""));
    }

    @Test
    void should_Throw_DateTimeParseException_GetSlotDateTimeFormatter_From_Invalid_Data() {

        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getLocalDateFormatter.apply(localDate_Invalid_String_Format_1));
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getLocalDateFormatter.apply(localDate_Invalid_String_Format_2));
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getLocalDateFormatter.apply(localDate_Invalid_String_Format_3));
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getLocalDateFormatter.apply(localDate_Invalid_String_Format_4));
        Assert.assertThrows(DateTimeParseException.class, () -> GenericFunctions.getLocalDateFormatter.apply(localDate_Invalid_String_Format_5));

    }

    @Test
    void should_GetSlotDateTimeFormatter_From_Valid_String() {

        LocalDate localDate = GenericFunctions.getLocalDateFormatter.apply(localDate_Valid_String_Format);
        Assert.assertEquals("2022-10-10", localDate.toString());

    }

}
