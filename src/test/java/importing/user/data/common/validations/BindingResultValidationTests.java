package importing.user.data.common.validations;

import importing.user.data.Application;
import importing.user.data.data.dto.ResponseDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.validation.BindingResult;
import testsupport.examples.AbstractExamples;
import testsupport.examples.BindingResultObjectExamples;
import testsupport.examples.UserDataObjectExamples;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class})
@ContextConfiguration
class BindingResultValidationTests extends AbstractExamples {

    @Autowired
    BindingResultValidation bindingResultValidation;

    /**
     * validateBindingResult by Binding Result
     */

    @Test
    void should_Return_Null_If_BindingResult_Has_No_Errors() {

        Assert.assertNull(bindingResultValidation.validateBindingResult(BindingResultObjectExamples.getBindingResult_No_Errors()));

    }

    @Test
    void should_Return_Errors_If_BindingResult_Has_Errors() {

        BindingResult bindingResult = BindingResultObjectExamples.getBindingResult_Errors();
        ResponseEntity<ResponseDTO> responseResponseEntity = bindingResultValidation.validateBindingResult(bindingResult);
        Assert.assertNotNull(responseResponseEntity);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, responseResponseEntity.getStatusCode());

    }

    /**
     * validateBindingResult by Object with Errors
     */

    @Test
    void should_Return_Errors_If_Object_Has_Binding_Result_Errors() {

        //Email is Null
        Optional<String> errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getInvalidUserData_Object_Email_Null());
        Assert.assertTrue(errors.isPresent());

        //Email is Empty
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getInvalidUserData_Object_Email_Empty());
        Assert.assertTrue(errors.isPresent());

        //Email is Invalid_1
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getInvalidUserData_Object_Email_Invalid_1());
        Assert.assertTrue(errors.isPresent());

        //Email is Invalid_2
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getInvalidUserData_Object_Email_Invalid_2());
        Assert.assertTrue(errors.isPresent());

        //First Name is Invalid
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_First_Name_Invalid());
        Assert.assertTrue(errors.isPresent());

        //Last Name is Invalidg
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Last_Name_Invalid());
        Assert.assertTrue(errors.isPresent());

        //Description is Invalid
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Description_Invalid());
        Assert.assertTrue(errors.isPresent());

        //Last Access Date is Invalid
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Last_Access_Date_Invalid());
        Assert.assertTrue(errors.isPresent());

        //Fiscal Code is Invalid
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Fiscal_Code_Invalid());
        Assert.assertTrue(errors.isPresent());

    }

    @Test
    void should_Return_No_Errors_If_Object_Has_Binding_Result_Valid() {

        //Email is Valid - Only Email
        Optional<String> errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Email_Valid());
        Assert.assertFalse(errors.isPresent());

        //First Name is Valid
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_First_Name_Valid());
        Assert.assertFalse(errors.isPresent());

        //Last Name is Validg
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Last_Name_Valid());
        Assert.assertFalse(errors.isPresent());

        //Description is Valid
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Description_Valid());
        Assert.assertFalse(errors.isPresent());

        //Last Access Date is Valid
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Last_Access_Date_Valid());
        Assert.assertFalse(errors.isPresent());

        //Fiscal Code is Valid
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Fiscal_Code_Valid());
        Assert.assertFalse(errors.isPresent());

        //User Data Full Object is Valid
        errors = bindingResultValidation.validateBindingResultErrorsFromDto(UserDataObjectExamples.getValidUserData_Object_Valid_1());
        Assert.assertFalse(errors.isPresent());

    }


}
