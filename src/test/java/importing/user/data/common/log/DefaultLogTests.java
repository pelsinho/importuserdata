package importing.user.data.common.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import importing.user.data.common.enums.ImportValidationPhase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;
import testsupport.helpers.CoreHelpMethods;

import java.util.List;

@ExtendWith(SpringExtension.class)
class DefaultLogTests extends AbstractExamples {


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldGenerateDefaultLogError() {

        List<ILoggingEvent> loggingEventList = CoreHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.ERROR);

        DefaultLog.defaultLogError(new Exception());

        CoreHelpMethods.loggingEventsContainsString.accept(loggingEventList, "- StackTrace :");


    }



    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldGenerateValidationLogError() {

        List<ILoggingEvent> loggingEventList = CoreHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.ERROR);

        DefaultLog.validationLogError(ImportValidationPhase.FILE_VALIDATION, "errorMsg");

        CoreHelpMethods.loggingEventsContainsString.accept(loggingEventList, "Error Messages");

    }


}
