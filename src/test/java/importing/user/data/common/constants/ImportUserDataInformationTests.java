package importing.user.data.common.constants;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;

import java.util.regex.Pattern;

import static importing.user.data.common.constants.ImportUserDataInformation.LAST_ACCESS_DATE_FORMAT;
import static importing.user.data.common.constants.ImportUserDataInformation.LAST_ACCESS_DATE_PATTERN;


@ExtendWith(SpringExtension.class)
class ImportUserDataInformationTests extends AbstractExamples {

    /**
     * SLOT_DATE_TIME_FORMAT
     */

    @Test
    void should_Be_Valid_Slot_Date_Time_Format() {

        Assert.assertEquals("dd-MM-yyyy", LAST_ACCESS_DATE_FORMAT);

    }

    @Test
    void should_Be_Invalid_Slot_Date_Time_Format() {

        Assert.assertNotEquals("dd/MM/yyyy/HH", LAST_ACCESS_DATE_FORMAT);
        Assert.assertNotEquals("dd-MM-yyyy HH", LAST_ACCESS_DATE_FORMAT);
        Assert.assertNotEquals("dd-MM-yyyy-HH", LAST_ACCESS_DATE_FORMAT);
        Assert.assertNotEquals("dd-MM-yyyy-H", LAST_ACCESS_DATE_FORMAT);
        Assert.assertNotEquals(null, LAST_ACCESS_DATE_FORMAT);
        Assert.assertNotEquals("", LAST_ACCESS_DATE_FORMAT);

    }


    /**
     * LAST_ACCESS_DATE_FORMAT_REGEX
     */

    @Test
    void should_Be_Valid_Last_Access_Date_Regex_Format() {

        Assert.assertTrue(Pattern.compile(LAST_ACCESS_DATE_PATTERN).matcher(localDate_Valid_String_Format).find());

    }

    @Test
    void should_Be_Invalid_Last_Access_Date_Regex_Format() {

        Assert.assertFalse(Pattern.compile(LAST_ACCESS_DATE_PATTERN).matcher(localDate_Invalid_String_Format_1).find());
        Assert.assertFalse(Pattern.compile(LAST_ACCESS_DATE_PATTERN).matcher(localDate_Invalid_String_Format_2).find());
        Assert.assertFalse(Pattern.compile(LAST_ACCESS_DATE_PATTERN).matcher(localDate_Invalid_String_Format_3).find());
        Assert.assertFalse(Pattern.compile(LAST_ACCESS_DATE_PATTERN).matcher(localDate_Invalid_String_Format_4).find());
        Assert.assertFalse(Pattern.compile(LAST_ACCESS_DATE_PATTERN).matcher(localDate_Invalid_String_Format_5).find());

    }


}
