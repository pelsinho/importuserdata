package importing.user.data.common.predicates;

import importing.user.data.Application;
import importing.user.data.common.constants.ImportUserDataInformation;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;
import testsupport.examples.MultiPartFileObjectExamples;

import static importing.user.data.common.constants.ImportUserDataInformation.FILE_LIMIT_MAX_SIZE_IN_KB;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class})
@TestPropertySource(properties = {
        "file.limit.size.in.kb=100",
})
@ContextConfiguration
class FilePredicatesTests extends AbstractExamples {

    @Autowired
    ImportUserDataInformation importUserDataInformation;

    /**
     * isFileHasAValidExtension
     */

    @Test
    void should_Return_True_If_File_Has_Valid_Extension() {

        Assert.assertFalse(FilePredicates.isFileHasAInvalidExtension.test(MultiPartFileObjectExamples.getMultiPartFile_Valid_Name_Extension()));

    }

    @Test
    void should_Return_True_If_File_Has_Invalid_Extension() {

        Assert.assertTrue(FilePredicates.isFileHasAInvalidExtension.test(MultiPartFileObjectExamples.getMultiPartFile_Invalid_Name_Extension()));

    }

    @Test
    void should_Return_True_If_File_Has_Invalid_Extension_Null() {

        Assert.assertTrue(FilePredicates.isFileHasAInvalidExtension.test(MultiPartFileObjectExamples.getMultiPartFile_Invalid_Name_Null()));

    }

    @Test
    void should_Return_True_If_File_Has_Invalid_Extension_Empty() {

        Assert.assertTrue(FilePredicates.isFileHasAInvalidExtension.test(MultiPartFileObjectExamples.getMultiPartFile_Invalid_Name_Empty()));

    }


    /**
     * isFileHasAValidSize
     */

    @Test
    void should_Return_False_If_File_Has_Valid_Size() {

        Assert.assertFalse(FilePredicates.isFileHasAInvalidSize.test(MultiPartFileObjectExamples.getMultiPartFile_Valid_Size(FILE_LIMIT_MAX_SIZE_IN_KB)));

    }

    @Test
    void should_Return_True_If_File_Has_Invalid_Size() {

        Assert.assertTrue(FilePredicates.isFileHasAInvalidSize.test(MultiPartFileObjectExamples.getMultiPartFile_Invalid_Size(FILE_LIMIT_MAX_SIZE_IN_KB)));

    }

}
