package importing.user.data.common.predicates;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;
import testsupport.examples.UserDataObjectExamples;

import java.util.List;

@ExtendWith(SpringExtension.class)
class UserDataPredicatesTests extends AbstractExamples {

    @Test
    void should_Return_True_If_UserDataList_Is_Null() {

        Assert.assertTrue(UserDataPredicates.thereIsNoDuplicateUserEmail.test(
                List.of(
                        UserDataObjectExamples.getValidUserData_Object_Valid_1(),
                        UserDataObjectExamples.getValidUserData_Object_Valid_2())));

    }


    @Test
    void should_Return_True_If_UserDataList_Is_Empty() {

        Assert.assertTrue(UserDataPredicates.thereIsNoDuplicateUserEmail.test(
                List.of(
                        UserDataObjectExamples.getValidUserData_Object_Valid_1(),
                        UserDataObjectExamples.getValidUserData_Object_Valid_2())));

    }


    @Test
    void should_Return_True_If_UserDataList_No_Duplicate_Emails() {

        Assert.assertTrue(UserDataPredicates.thereIsNoDuplicateUserEmail.test(
                List.of(
                        UserDataObjectExamples.getValidUserData_Object_Valid_1(),
                        UserDataObjectExamples.getValidUserData_Object_Valid_2())));

    }

    @Test
    void should_Return_False_If_UserDataList_Has_Duplicate_Emails() {

        Assert.assertFalse(UserDataPredicates.thereIsNoDuplicateUserEmail.test(
                List.of(
                        UserDataObjectExamples.getValidUserData_Object_Valid_1(),
                        UserDataObjectExamples.getValidUserData_Object_Valid_1(),
                        UserDataObjectExamples.getValidUserData_Object_Valid_2())));

    }

}
