package importing.user.data.data.mappers;

import importing.user.data.data.entities.UserDataDB;
import importing.user.data.data.model.UserData;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.UserDataObjectExamples;
import testsupport.validation.ValidateObject;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
class UserDataToUserDBMapperTests {

    @Test
    void should_Get_Empty_List_userDataListToUserDBList_Null() {

        List<UserDataDB> userDataDBList = UserDataToUserDBMapper.userDataListToUserDBList(null);
        Assert.assertNotNull(userDataDBList);
        Assert.assertTrue(userDataDBList.isEmpty());

    }

    @Test
    void should_Get_Empty_List_userDataListToUserDBList_Empty() {

        List<UserDataDB> userDataDBList = UserDataToUserDBMapper.userDataListToUserDBList(new ArrayList<>());
        Assert.assertNotNull(userDataDBList);
        Assert.assertTrue(userDataDBList.isEmpty());
    }

    @Test
    void should_Get_Valid_List_userDataListToUserDBList() {

        //Get UserData Object
        List<UserData> userDataList = List.of(UserDataObjectExamples.getValidUserData_Object_Valid_1());

        //Perform Mapper to UserData DB
        List<UserDataDB> userDataDBList = UserDataToUserDBMapper.userDataListToUserDBList(userDataList);

        //Validate it
        ValidateObject.validateUserDataDBxUserData(userDataDBList, userDataList);

    }

}
