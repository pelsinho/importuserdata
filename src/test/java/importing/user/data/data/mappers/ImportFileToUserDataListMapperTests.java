package importing.user.data.data.mappers;

import importing.user.data.data.model.UserData;
import importing.user.data.exceptions.ImportFileToUserDataException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.MultiPartFileObjectExamples;

import java.util.List;

@ExtendWith(SpringExtension.class)
class ImportFileToUserDataListMapperTests {

    @Test
    void should_Throw_ImportFileToUserDataException_importFileToUserDataList_Null() throws ImportFileToUserDataException {

        List<UserData> userDataList = ImportFileToUserDataListMapper.importFileToUserDataList(null);
        Assert.assertNotNull(userDataList);
        Assert.assertTrue(userDataList.isEmpty());

    }

    @Test
    void should_Throw_ImportFileToUserDataException_importFileToUserDataList_Empty() throws ImportFileToUserDataException {

        List<UserData> userDataList = ImportFileToUserDataListMapper.importFileToUserDataList(MultiPartFileObjectExamples.getMultiPartFile_Invalid_Name_Null());
        Assert.assertNotNull(userDataList);
        Assert.assertTrue(userDataList.isEmpty());

    }

}
