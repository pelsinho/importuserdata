package importing.user.data.data.mappers;

import importing.user.data.data.dto.UserDataDTO;
import importing.user.data.data.entities.UserDataDB;
import importing.user.data.data.model.UserData;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.UserDataObjectExamples;
import testsupport.validation.ValidateObject;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
class UserDBToUserDataDTOMapperTests {

    @Test
    void should_Get_Empty_List_userDataListToUserDTOList_Null() {

        List<UserDataDTO> userDataDTOList = UserDataDBToUserDataDTOMapper.userDataListToUserDTOList(null);
        Assert.assertNotNull(userDataDTOList);
        Assert.assertTrue(userDataDTOList.isEmpty());

    }

    @Test
    void should_Get_Empty_List_userDataListToUserDTOList_Empty() {

        List<UserDataDTO> userDataDTOList = UserDataDBToUserDataDTOMapper.userDataListToUserDTOList(new ArrayList<>());
        Assert.assertNotNull(userDataDTOList);
        Assert.assertTrue(userDataDTOList.isEmpty());
    }

    @Test
    void should_Get_Valid_List_userDataListToUserDTOList() {

        //Get UserData Object
        List<UserData> userDataList = List.of(UserDataObjectExamples.getValidUserData_Object_Valid_1());

        //Perform Mapper to UserData DB
        List<UserDataDB> userDataDBList = UserDataToUserDBMapper.userDataListToUserDBList(userDataList);

        //Mapper to DTO
        List<UserDataDTO> userDataDTOList = UserDataDBToUserDataDTOMapper.userDataListToUserDTOList(userDataDBList);

        //Validate it
        ValidateObject.validateUserDataDBxUserDataDTO(userDataDBList, userDataDTOList);

    }

}
