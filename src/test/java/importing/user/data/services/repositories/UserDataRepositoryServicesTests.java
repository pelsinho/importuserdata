package importing.user.data.services.repositories;

import importing.user.data.Application;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;
import testsupport.examples.UserDataObjectExamples;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class})
@ContextConfiguration
class UserDataRepositoryServicesTests extends AbstractExamples {

    @Autowired
    UserDataRepositoryServices userDataRepositoryServices;



    @Test
    void should_Do_Not_Thrown_Any_Exceptions_If_UserDataList_Is_Null() {

        try {

            userDataRepositoryServices.saveUserData(null);

        } catch (Exception e) {

            fail();

        }

    }


    @Test
    void should_Do_Not_Thrown_Any_Exceptions_If_UserDataList_Is_Empty() {

        try {

            userDataRepositoryServices.saveUserData(new ArrayList<>());

        } catch (Exception e) {

            fail();

        }

    }

    @Test
    void should_Do_Not_Thrown_Any_Exceptions_If_UserDataList_Is_Valid() {

        try {

            userDataRepositoryServices.saveUserData(
                    List.of(
                            UserDataObjectExamples.getValidUserData_Object_Valid_1(),
                            UserDataObjectExamples.getValidUserData_Object_Valid_2()));

        } catch (Exception e) {

            fail();

        }

    }



}
