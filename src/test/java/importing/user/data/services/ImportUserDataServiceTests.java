package importing.user.data.services;

import importing.user.data.Application;
import importing.user.data.data.dto.UserDataDTO;
import importing.user.data.exceptions.ImportFileToUserDataException;
import importing.user.data.exceptions.UserImportDataValidationException;
import importing.user.data.services.repositories.UserDataRepositoryServices;
import importing.user.data.services.validations.ImportUserDataValidationProcess;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;
import testsupport.examples.MultiPartFileObjectExamples;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class})
@ContextConfiguration
class ImportUserDataServiceTests extends AbstractExamples {

    private Example src = new Example();

    @Autowired
    ImportUserDataService importUserDataService;

    @MockBean
    ImportUserDataValidationProcess importUserDataValidationProcess;

    @MockBean
    UserDataRepositoryServices userDataRepositoryServices;

    @Test
    void should_ThrowUserImportDataValidationException_if_ImportUserDataService_With_Null() throws ImportFileToUserDataException, UserImportDataValidationException {

        List<UserDataDTO> userDataDTOList = importUserDataService.importUserDataService(null);
        Assert.assertNotNull(userDataDTOList);
        Assert.assertTrue(userDataDTOList.isEmpty());

    }

    @Test
    void should_ThrowUserImportDataValidationException_if_ImportUserDataService_With_Empty() throws ImportFileToUserDataException, UserImportDataValidationException {

        List<UserDataDTO> userDataDTOList = importUserDataService.importUserDataService(MultiPartFileObjectExamples.getMultiPartFile_Invalid_Name_Null());
        Assert.assertNotNull(userDataDTOList);
        Assert.assertTrue(userDataDTOList.isEmpty());

    }


}
