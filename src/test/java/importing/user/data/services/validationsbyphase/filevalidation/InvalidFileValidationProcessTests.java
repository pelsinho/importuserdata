package importing.user.data.services.validationsbyphase.filevalidation;

import importing.user.data.Application;
import importing.user.data.common.enums.ImportValidationPhase;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.exceptions.UserImportDataValidationException;
import importing.user.data.services.validations.ImportUserDataValidationProcess;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;
import testsupport.examples.MultiPartFileObjectExamples;

import static importing.user.data.common.constants.ImportUserDataInformation.FILE_LIMIT_MAX_SIZE_IN_KB;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class})
@ContextConfiguration
class InvalidFileValidationProcessTests extends AbstractExamples {

    /**
     * This Test will cover de entire Validation Process Integration
     * The predicate methods used to validate the information were been tested in tests-importing-user-data-common-predicates folder
     * No connection to DB during this process
     * All this tests will throw UserImportDataValidationException
     */

    @Autowired
    private ImportUserDataValidationProcess importUserDataValidationProcess;

    @Test
    void should_ThrowUserImportDataValidationException_When_Send_Null_Empty() {

        //Cannot be Empty
        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .multipartFile(MultiPartFileObjectExamples.getMultiPartFile_Invalid_Name_Null())
                        .importValidationPhase(ImportValidationPhase.FILE_VALIDATION)
                        .build()
        ));

        //Cannot be Null
        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .multipartFile(null)
                        .importValidationPhase(ImportValidationPhase.FILE_VALIDATION)
                        .build()
        ));

    }


    /**
     * Validate UserDataHasDuplicateEmailsHandle
     */

    @Test
    void should_ThrowUserImportDataValidationException_When_Send_Data_With_Invalid_Name_Extension()  {

        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .multipartFile(MultiPartFileObjectExamples.getMultiPartFile_Invalid_Name_Extension())
                        .importValidationPhase(ImportValidationPhase.FILE_VALIDATION)
                        .build()
        ));

    }


    /**
     * Validate UserDataFieldsAreValidHandle
     */

    @Test
    void should_ThrowUserImportDataValidationException_When_Send_File_Over_Size_Limit()  {

        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .multipartFile(MultiPartFileObjectExamples.getMultiPartFile_Invalid_Size(FILE_LIMIT_MAX_SIZE_IN_KB))
                        .importValidationPhase(ImportValidationPhase.FILE_VALIDATION)
                        .build()));

    }




}
