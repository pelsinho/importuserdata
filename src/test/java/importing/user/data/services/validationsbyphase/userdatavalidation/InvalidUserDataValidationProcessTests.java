package importing.user.data.services.validationsbyphase.userdatavalidation;

import importing.user.data.Application;
import importing.user.data.common.enums.ImportValidationPhase;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.exceptions.UserImportDataValidationException;
import importing.user.data.services.validations.ImportUserDataValidationProcess;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;
import testsupport.examples.UserDataObjectExamples;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class})
@ContextConfiguration
class InvalidUserDataValidationProcessTests extends AbstractExamples {

    /**
     * This Test will cover de entire Validation Process Integration
     * The predicate methods used to validate the information were been tested in tests-importing-user-data-common-predicates folder
     * No connection to DB during this process
     * All this tests will throw UserImportDataValidationException
     */

    @Autowired
    private ImportUserDataValidationProcess importUserDataValidationProcess;

    @Test
    void should_ThrowUserImportDataValidationException_When_Send_Null_Empty() {

        //Cannot be Null
        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .userDataList(null)
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build()
        ));

        //Cannot be Empty
        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .userDataList(new ArrayList<>())
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build()
        ));

    }


    /**
     * Validate UserDataHasDuplicateEmailsHandle
     */

    @Test
    void should_ThrowUserImportDataValidationException_When_Send_Data_With_Duplicate_Emails()  {

        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .userDataList(List.of(
                                UserDataObjectExamples.getValidUserData_Object_Valid_1(),
                                UserDataObjectExamples.getValidUserData_Object_Valid_1(),
                                UserDataObjectExamples.getValidUserData_Object_Valid_2()))
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build()
        ));

    }


    /**
     * Validate UserDataFieldsAreValidHandle
     */

    @Test
    void should_ThrowUserImportDataValidationException_When_Send_Invalid_Data()  {

        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .userDataList(List.of(
                                UserDataObjectExamples.getInvalidUserData_Object_Email_Invalid_1(),
                                UserDataObjectExamples.getValidUserData_Object_Valid_2()))
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build()
        ));

        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .userDataList(List.of(
                                UserDataObjectExamples.getValidUserData_Object_Valid_2(),
                                UserDataObjectExamples.getValidUserData_Object_Last_Name_Invalid()))
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build()
        ));

        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .userDataList(List.of(
                                UserDataObjectExamples.getValidUserData_Object_Valid_2(),
                                UserDataObjectExamples.getValidUserData_Object_Last_Access_Date_Invalid()))
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build()
        ));

        Assert.assertThrows(UserImportDataValidationException.class, () -> importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .userDataList(List.of(
                                UserDataObjectExamples.getValidUserData_Object_Valid_2(),
                                UserDataObjectExamples.getValidUserData_Object_Fiscal_Code_Invalid()))
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build()
        ));

    }




}
