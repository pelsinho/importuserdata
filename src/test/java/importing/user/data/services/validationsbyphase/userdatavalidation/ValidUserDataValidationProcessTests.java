package importing.user.data.services.validationsbyphase.userdatavalidation;

import importing.user.data.Application;
import importing.user.data.common.enums.ImportValidationPhase;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.exceptions.UserImportDataValidationException;
import importing.user.data.services.validations.ImportUserDataValidationProcess;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import testsupport.examples.AbstractExamples;
import testsupport.examples.UserDataObjectExamples;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class})
@ContextConfiguration
class ValidUserDataValidationProcessTests extends AbstractExamples {

    /**
     * This Test will cover de entire Validation Process Integration
     * The predicate methods used to validate the information were been tested in testes-api-common-predicates folder
     * No connection to DB during this process
     * All this tests will throw UserImportDataValidationException
     */

    @Autowired
    private ImportUserDataValidationProcess importUserDataValidationProcess;

    @Test
    void should_Not_ThrowAnyException_When_Send_Valid_Data() throws UserImportDataValidationException {

        importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .userDataList(List.of(UserDataObjectExamples.getValidUserData_Object_Valid_1()))
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build());

        importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest
                        .builder()
                        .userDataList(List.of(UserDataObjectExamples.getValidUserData_Object_Valid_2()))
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build());

    }


}
