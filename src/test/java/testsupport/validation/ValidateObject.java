package testsupport.validation;


import functionaltests.common.enums.VersionFiles;
import importing.user.data.common.functions.GenericFunctions;
import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.data.dto.UserDataDTO;
import importing.user.data.data.entities.UserDataDB;
import importing.user.data.data.model.UserData;
import org.junit.Assert;

import java.util.List;

public class ValidateObject {


    public static void validateUserDataDB(List<UserDataDB> receivedUserDataDBList, List<UserDataDB> expectedUserDataDBList) {

        /**
         * Basic Not Null Validation
         */
        Assert.assertNotNull(receivedUserDataDBList);
        Assert.assertNotNull(expectedUserDataDBList);

        /**
         * Same Size
         */
        Assert.assertEquals(receivedUserDataDBList.size(), expectedUserDataDBList.size());

        /**
         * Validate Same Object
         */

        for (int i=0; i<receivedUserDataDBList.size(); i++) {

            //Always has a Value, cannot be NULL
            Assert.assertEquals(receivedUserDataDBList.get(i).getEmail(), expectedUserDataDBList.get(i).getEmail());

            //Description
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(receivedUserDataDBList.get(i).getDescription()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(expectedUserDataDBList.get(i).getDescription())) {
                Assert.assertEquals(receivedUserDataDBList.get(i).getDescription(), expectedUserDataDBList.get(i).getDescription());
            }

            //FirstName
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(receivedUserDataDBList.get(i).getFirstName()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(expectedUserDataDBList.get(i).getFirstName())) {
                Assert.assertEquals(receivedUserDataDBList.get(i).getFirstName(), expectedUserDataDBList.get(i).getFirstName());
            }

            //LastName
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(receivedUserDataDBList.get(i).getLastName()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(expectedUserDataDBList.get(i).getLastName())) {
                Assert.assertEquals(receivedUserDataDBList.get(i).getLastName(), expectedUserDataDBList.get(i).getLastName());
            }

            //Fiscal Code
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(receivedUserDataDBList.get(i).getFiscalCode()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(expectedUserDataDBList.get(i).getFiscalCode())) {
                Assert.assertEquals(receivedUserDataDBList.get(i).getFiscalCode(), expectedUserDataDBList.get(i).getFiscalCode());
            }

            //Last Access Date
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(receivedUserDataDBList.get(i).getLastAccessDate()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(expectedUserDataDBList.get(i).getLastAccessDate())) {
                Assert.assertEquals(receivedUserDataDBList.get(i).getLastAccessDate(), expectedUserDataDBList.get(i).getLastAccessDate());
            }

        }


    }


    public static void validateUserDataDBxUserData(List<UserDataDB> userDataDBList, List<UserData> userDataList) {

        /**
         * Basic Not Null Validation
         */
        Assert.assertNotNull(userDataDBList);
        Assert.assertNotNull(userDataList);

        /**
         * Same Size
         */
        Assert.assertEquals(userDataDBList.size(), userDataList.size());

        /**
         * Validate Same Object
         */

        for (int i=0; i<userDataDBList.size(); i++) {

            //Always has a Value, cannot be NULL
            Assert.assertEquals(userDataDBList.get(i).getEmail(), userDataList.get(i).getEmail());

            //Description
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getDescription()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getDescription())) {
                Assert.assertEquals(userDataDBList.get(i).getDescription(), userDataList.get(i).getDescription());
            }

            //FirstName
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getFirstName()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getFirstName())) {
                Assert.assertEquals(userDataDBList.get(i).getFirstName(), userDataList.get(i).getFirstName());
            }

            //LastName
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getLastName()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getLastName())) {
                Assert.assertEquals(userDataDBList.get(i).getLastName(), userDataList.get(i).getLastName());
            }

            //Fiscal Code
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getFiscalCode()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getFiscalCode())) {
                Assert.assertEquals(userDataDBList.get(i).getFiscalCode(), userDataList.get(i).getFiscalCode());
            }

            //Last Access Date
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getLastAccessDate()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataList.get(i).getLastAccessDate())) {
                Assert.assertEquals(userDataDBList.get(i).getLastAccessDate(), GenericFunctions.getLocalDateFormatter.apply(userDataList.get(i).getLastAccessDate()));
            }

        }


    }


    public static void validateUserDataDBxUserDataDTO(List<UserDataDB> userDataDBList, List<UserDataDTO> userDataDTOList) {

        /**
         * Basic Not Null Validation
         */
        Assert.assertNotNull(userDataDBList);
        Assert.assertNotNull(userDataDTOList);

        /**
         * Same Size
         */
        Assert.assertEquals(userDataDBList.size(), userDataDTOList.size());

        /**
         * Validate Same Object
         */

        for (int i = 0; i < userDataDBList.size(); i++) {

            //Always has a Value, cannot be NULL
            Assert.assertEquals(userDataDBList.get(i).getEmail(), userDataDTOList.get(i).getEmail());

            //Description
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getDescription()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getDescription())) {
                Assert.assertEquals(userDataDBList.get(i).getDescription(), userDataDTOList.get(i).getDescription());
            }

            //FirstName
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getFirstName()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getFirstName())) {
                Assert.assertEquals(userDataDBList.get(i).getFirstName(), userDataDTOList.get(i).getFirstName());
            }

            //LastName
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getLastName()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getLastName())) {
                Assert.assertEquals(userDataDBList.get(i).getLastName(), userDataDTOList.get(i).getLastName());
            }

            //Fiscal Code
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getFiscalCode()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getFiscalCode())) {
                Assert.assertEquals(userDataDBList.get(i).getFiscalCode(), userDataDTOList.get(i).getFiscalCode());
            }

            //Last Access Date
            //If Both are Null, we dont need to check
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getLastAccessDate()) &&
                    GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getLastAccessDate())) {
                Assert.assertEquals(userDataDBList.get(i).getLastAccessDate(), userDataDTOList.get(i).getLastAccessDate());
            }

        }

    }



    public static void validateUserDataDTO(
            List<UserDataDTO> userDataDTOList,
            Boolean emailPresent,
            Boolean lastNamePresent,
            Boolean firstNamePresent,
            Boolean fiscalCodePresent,
            Boolean descriptionPresent,
            Boolean lastAccessDatePresent) {

        /**
         * Basic Not Null Validation
         */
        Assert.assertNotNull(userDataDTOList);
        Assert.assertFalse(userDataDTOList.isEmpty());


        /**
         * Validate Same Object
         */

        for (int i=0; i<userDataDTOList.size(); i++) {

            //Email
            if (emailPresent) {
                Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getEmail()));
            }

            //Description
            if (descriptionPresent) {
                Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getDescription()));
            }

            //FirstName
            if (firstNamePresent) {
                Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getFirstName()));
            }

            //LastName
            if (lastNamePresent) {
                Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getLastName()));
            }

            //Fiscal Code
            if (fiscalCodePresent) {
                Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getFiscalCode()));
            }

            //Last Access Date
            if (lastAccessDatePresent) {
                Assert.assertTrue(GenericPredicates.checkIfNullOrEmpty.negate().test(userDataDTOList.get(i).getLastAccessDate()));
            }

        }

    }


    public static void validateUserDataDTOHasCorrectTestVersion(
            List<UserDataDTO> userDataDTOList,
            VersionFiles versionFiles) {

        Assert.assertTrue(userDataDTOList
                .stream()
                .noneMatch(x -> x.getDescription().contains(versionFiles.name())));

        Assert.assertTrue(userDataDTOList
                .stream()
                .noneMatch(x -> x.getFirstName().contains(versionFiles.name())));

        Assert.assertTrue(userDataDTOList
                .stream()
                .noneMatch(x -> x.getLastName().contains(versionFiles.name())));

    }


    public static void validateUserDataDBHasCorrectTestVersion(
            List<UserDataDB> userDataDBList,
            VersionFiles versionFiles) {

        Assert.assertTrue(userDataDBList
                .stream()
                .noneMatch(x -> x.getDescription().contains(versionFiles.name())));

        Assert.assertTrue(userDataDBList
                .stream()
                .noneMatch(x -> x.getFirstName().contains(versionFiles.name())));

        Assert.assertTrue(userDataDBList
                .stream()
                .noneMatch(x -> x.getLastName().contains(versionFiles.name())));

    }


}
