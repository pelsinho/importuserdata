package testsupport.examples;


import importing.user.data.common.functions.GenericFunctions;

import java.time.LocalDate;

public class AbstractExamples {


    /**
     * TEST TAGS
     */

    public static final String TAG_FUNCTIONAL = "functional";

    /**
     * Local Date String
     */

    public static final String localDate_Valid_String_Format = "10-10-2022";

    public static final String localDate_Invalid_String_Format_1 = "40-12-2020-22";
    public static final String localDate_Invalid_String_Format_2 = "20-12-2020-22:00";
    public static final String localDate_Invalid_String_Format_3 = "2000-12-20-22";
    public static final String localDate_Invalid_String_Format_4 = "30-13-2020-22";
    public static final String localDate_Invalid_String_Format_5 = "30-12-2020-12";


    /**
     * Local Date
     */

    public static final LocalDate localDate_Valid = GenericFunctions.getLocalDateFormatter.apply(localDate_Valid_String_Format);


    /**
     * MultiPartFile
     */

    public static final String multiPartFile_Name_Valid_Extension_1 = "MyMultiPartFileNameIsValid.csv";
    public static final String multiPartFile_Name_Invalid_Extension_1 = "MyMultiPartFileNameIsValid.txt";



    /**
     * User Data Objects
     */

    public static final String EMAIL_INVALID_PATTERN_1 = "email@gmail";
    public static final String EMAIL_INVALID_PATTERN_2 = "emailgmail.com";
    public static final String FIRST_NAME_INVALID_PATTERN_1 = "Until100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGo";
    public static final String LAST_NAME_INVALID_PATTERN_1 = "Until100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGo";
    public static final String DESCRIPTION_INVALID_PATTERN_1 = "Until100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGoUntil100LestGo";
    public static final String LAST_ACCESS_DATE_INVALID_PATTERN_1 = "22/10/2020";
    public static final String FISCAL_CODE_INVALID_PATTERN_1 = "234@223.233-5";

    public static final String EMAIL_VALID_PATTERN_1 = "email@gmail.com";
    public static final String EMAIL_VALID_PATTERN_2 = "secondemail@gmail.com";
    public static final String FIRST_NAME_VALID_PATTERN_1 = "Nelson";
    public static final String LAST_NAME_VALID_PATTERN_1 = "Gomes";
    public static final String DESCRIPTION_VALID_PATTERN_1 = "My Name is Nelson Gomes. 123 is my Luck Number";
    public static final String LAST_ACCESS_DATE_VALID_PATTERN_1 = localDate_Valid_String_Format;
    public static final String FISCAL_CODE_VALID_PATTERN_1 = "234.223_233-5";



    public class Example {




    }

}