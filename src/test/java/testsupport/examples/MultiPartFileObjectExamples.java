package testsupport.examples;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static testsupport.examples.AbstractExamples.multiPartFile_Name_Invalid_Extension_1;
import static testsupport.examples.AbstractExamples.multiPartFile_Name_Valid_Extension_1;

public class MultiPartFileObjectExamples {

    /**
     * MultiPart Size Examples
     * @param fileLimitMaxSizeInKb
     */

    public static MultipartFile getMultiPartFile_Valid_Size(Integer fileLimitMaxSizeInKb) {

        MultipartFile multipartFile = new MultipartFile() {
            @Override
            public String getName() {
                return multiPartFile_Name_Valid_Extension_1;
            }

            @Override
            public String getOriginalFilename() {
                return multiPartFile_Name_Valid_Extension_1;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return fileLimitMaxSizeInKb / 2;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[fileLimitMaxSizeInKb / 2];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };

        return multipartFile;
    }

    public static MultipartFile getMultiPartFile_Invalid_Size(Integer fileLimitMaxSizeInKb) {

        MultipartFile multipartFile = new MultipartFile() {
            @Override
            public String getName() {
                return multiPartFile_Name_Invalid_Extension_1;
            }

            @Override
            public String getOriginalFilename() {
                return multiPartFile_Name_Invalid_Extension_1;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return fileLimitMaxSizeInKb * 2;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };

        return multipartFile;
    }


    /**
     * MultiPart Extension Examples
     */

    public static MultipartFile getMultiPartFile_Invalid_Name_Empty() {

        MultipartFile multipartFile = new MultipartFile() {
            @Override
            public String getName() {
                return "";
            }

            @Override
            public String getOriginalFilename() {
                return "";
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };

        return multipartFile;
    }

    public static MultipartFile getMultiPartFile_Invalid_Name_Null() {

        MultipartFile multipartFile = new MultipartFile() {
            @Override
            public String getName() {
                return null;
            }


            @Override
            public String getOriginalFilename() {
                return null;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };

        return multipartFile;
    }

    public static MultipartFile getMultiPartFile_Invalid_Name_Extension() {

        MultipartFile multipartFile = new MultipartFile() {
            @Override
            public String getName() {
                return multiPartFile_Name_Invalid_Extension_1;
            }

            @Override
            public String getOriginalFilename() {
                return multiPartFile_Name_Invalid_Extension_1;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 10;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };

        return multipartFile;
    }

    public static MultipartFile getMultiPartFile_Valid_Name_Extension() {

        MultipartFile multipartFile = new MultipartFile() {
            @Override
            public String getName() {
                return multiPartFile_Name_Valid_Extension_1;
            }

            @Override
            public String getOriginalFilename() {
                return multiPartFile_Name_Valid_Extension_1;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 10;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[10];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };

        return multipartFile;
    }



}
