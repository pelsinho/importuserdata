package testsupport.examples;

import importing.user.data.data.model.UserData;

import static testsupport.examples.AbstractExamples.*;

public class UserDataObjectExamples {

    /**
     * EMAIL FIELD VALIDATION
     */

    public static UserData getInvalidUserData_Object_Email_Null() {

        UserData userData = new UserData();
        userData.setEmail(null);
        return userData;

    }

    public static UserData getInvalidUserData_Object_Email_Empty() {

        UserData userData = new UserData();
        userData.setEmail("");
        return userData;

    }

    public static UserData getInvalidUserData_Object_Email_Invalid_1() {

        UserData userData = new UserData();
        userData.setEmail(EMAIL_INVALID_PATTERN_1);
        return userData;

    }

    public static UserData getInvalidUserData_Object_Email_Invalid_2() {

        UserData userData = new UserData();
        userData.setEmail(EMAIL_INVALID_PATTERN_2);
        return userData;

    }

    public static UserData getValidUserData_Object_Email_Valid() {

        UserData userData = new UserData();
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    /**
     * FIRST NAME FIELD VALIDATION
     */

    public static UserData getValidUserData_Object_First_Name_Invalid() {

        UserData userData = new UserData();
        userData.setFirstName(FIRST_NAME_INVALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    public static UserData getValidUserData_Object_First_Name_Valid() {

        UserData userData = new UserData();
        userData.setFirstName(FIRST_NAME_VALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    /**
     * LAST NAME FIELD VALIDATION
     */

    public static UserData getValidUserData_Object_Last_Name_Invalid() {

        UserData userData = new UserData();
        userData.setLastName(LAST_NAME_INVALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    public static UserData getValidUserData_Object_Last_Name_Valid() {

        UserData userData = new UserData();
        userData.setLastName(LAST_NAME_VALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    /**
     * DESCRIPTION FIELD VALIDATION
     */

    public static UserData getValidUserData_Object_Description_Invalid() {

        UserData userData = new UserData();
        userData.setDescription(DESCRIPTION_INVALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    public static UserData getValidUserData_Object_Description_Valid() {

        UserData userData = new UserData();
        userData.setDescription(DESCRIPTION_VALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    /**
     * LAST ACCESS DATE FIELD VALIDATION
     */

    public static UserData getValidUserData_Object_Last_Access_Date_Invalid() {

        UserData userData = new UserData();
        userData.setLastAccessDate(LAST_ACCESS_DATE_INVALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    public static UserData getValidUserData_Object_Last_Access_Date_Valid() {

        UserData userData = new UserData();
        userData.setLastAccessDate(LAST_ACCESS_DATE_VALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    /**
     * FISCAL CODE FIELD VALIDATION
     */

    public static UserData getValidUserData_Object_Fiscal_Code_Invalid() {

        UserData userData = new UserData();
        userData.setFiscalCode(FISCAL_CODE_INVALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    public static UserData getValidUserData_Object_Fiscal_Code_Valid() {

        UserData userData = new UserData();
        userData.setFiscalCode(FISCAL_CODE_VALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    /**
     * FISCAL CODE FIELD VALIDATION
     */

    public static UserData getValidUserData_Object_Valid_1() {

        UserData userData = new UserData();
        userData.setLastAccessDate(LAST_ACCESS_DATE_VALID_PATTERN_1);
        userData.setDescription(DESCRIPTION_VALID_PATTERN_1);
        userData.setLastName(LAST_NAME_VALID_PATTERN_1);
        userData.setFirstName(FIRST_NAME_VALID_PATTERN_1);
        userData.setFiscalCode(FISCAL_CODE_VALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_1);
        return userData;

    }

    public static UserData getValidUserData_Object_Valid_2() {

        UserData userData = new UserData();
        userData.setLastAccessDate(LAST_ACCESS_DATE_VALID_PATTERN_1);
        userData.setDescription(DESCRIPTION_VALID_PATTERN_1);
        userData.setLastName(LAST_NAME_VALID_PATTERN_1);
        userData.setFirstName(FIRST_NAME_VALID_PATTERN_1);
        userData.setFiscalCode(FISCAL_CODE_VALID_PATTERN_1);
        userData.setEmail(EMAIL_VALID_PATTERN_2);
        return userData;

    }


}
