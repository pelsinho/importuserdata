package importing.user.data.controllers;

import importing.user.data.common.log.DefaultLog;
import importing.user.data.data.entities.UserDataDB;
import importing.user.data.exceptions.ExceptionType;
import importing.user.data.exceptions.ServiceException;
import importing.user.data.repositories.UserDataRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.models.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RequiredArgsConstructor
@RestController
@Profile("!prd")
@RequestMapping("/usertestdata")
public class UserDataDevController {

    private final UserDataRepository userDataRepository;


    @ApiOperation(value = "Get All User Data",
            notes =
            "<p> Important : </p>" +
            "<br> Important - This controller will NOT be available in production.</br>" +
            "<br> The objective is only to perform test during developer and release process.</br>")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid request", response = Response.class),
        @ApiResponse(code = 403, message = "Access Denied", response = Response.class),
        @ApiResponse(code = 401, message = "Unauthenticated", response = Response.class),
        @ApiResponse(code = 500, message = "Internal server error"),
        @ApiResponse(code = 422, message = "Unprocessable_entity")
    })
    @GetMapping
    public ResponseEntity<List<UserDataDB>> getAllUserData() {

        try {

            return ok(userDataRepository.findAll());

        } catch (Exception ex) {

            DefaultLog.defaultLogError(ex);
            throw new ServiceException(ExceptionType.FORBIDDEN, ex.getMessage());

        }

    }


    @ApiOperation(value = "Delete All User Data",
            notes =
                    "<p> Important : </p>" +
                            "<br> Important - This controller will NOT be available in production.</br>" +
                            "<br> The objective is only to perform test during developer and release process.</br>")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid request", response = Response.class),
            @ApiResponse(code = 403, message = "Access Denied", response = Response.class),
            @ApiResponse(code = 401, message = "Unauthenticated", response = Response.class),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 422, message = "Unprocessable_entity")
    })
    @DeleteMapping
    public ResponseEntity deleteAllUserData() {

        try {

            //Process with Import Process
            userDataRepository.deleteAll();
            return ok().build();

        } catch (Exception ex) {

            DefaultLog.defaultLogError(ex);
            throw new ServiceException(ExceptionType.FORBIDDEN, ex.getMessage());

        }

    }


}
