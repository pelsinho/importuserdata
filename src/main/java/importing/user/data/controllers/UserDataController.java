package importing.user.data.controllers;

import importing.user.data.common.log.DefaultLog;
import importing.user.data.common.validations.BindingResultValidation;
import importing.user.data.data.dto.UserDataDTO;
import importing.user.data.exceptions.ExceptionType;
import importing.user.data.exceptions.ServiceException;
import importing.user.data.services.ImportUserDataService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.models.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

import static importing.user.data.common.constants.ImportUserDataInformation.*;
import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/userdata")
public class UserDataController {

    private final ImportUserDataService importUserDataService;
    private final BindingResultValidation bindingResultValidation;


    @ApiOperation(value = "Import User Data",
            notes =
            "<p> - Please if you have any question please contact our support :</p>" +
            "<br></br>" +
            "<p>Each row contains such fields in the following order: Email, Last Name, First Name, Fiscal Code, Description, Last Access Date </p>" +
            "<br></br>" +
            "<p> - 1 - There cannot be users with the same email in the import file </p>" +
            "<p> - 2 - All fields (with the exception of email) can be null or empty.</p>" +
            "<p> - 3 - Any file is the new master. All values will be updated </p>" +
            "<br></br>" +
            "<p> - Regex Validation By Field </p>" +
            "<br></br>" +
            "<p> - 1 - Email</p>" +
            "<br> - Example : motork@support.com</p>" +
            "<br> - Regex Validation Pattern : " + EMAIL_VERIFICATION_PATTERN + "</p>" +
            "<br></br>" +
            "<p> - 2 - First Name, Last Name and Description - (Maximum Size : 100 chars)</p>" +
            "<br> - Examples : First Name : Nelson, Last Name : Gomes , Description : is doing this job</p>" +
            "<br></br>" +
            "<p> - 3 - Fiscal Code</p>" +
            "<br> - Example : 234.323.112-5</p>" +
            "<br> - Regex Validation Pattern : " + FISCAL_CODE_PATTERN + "</p>" +
            "<br></br>" +
            "<p> - 4 - Last Access Date</p>" +
            "<br> - Example : 31-11-2022</p>" +
            "<br> - Regex Validation Pattern : " + LAST_ACCESS_DATE_PATTERN + "</p>" +
            "<br></br>" +
            "<p> Important - For a better experience. Read API Instructions at th top of Swagger UI Home Page </p>")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Invalid request", response = Response.class),
        @ApiResponse(code = 403, message = "Access Denied", response = Response.class),
        @ApiResponse(code = 401, message = "Unauthenticated", response = Response.class),
        @ApiResponse(code = 500, message = "Internal server error"),
        @ApiResponse(code = 422, message = "Unprocessable_entity")
    })
    @PostMapping
    public ResponseEntity<List<UserDataDTO>> importUserData(@ModelAttribute @Valid MultipartFile file) {

        try {

            return ok(importUserDataService.importUserDataService(file));

        } catch (Exception ex) {

            DefaultLog.defaultLogError(ex);
            throw new ServiceException(ExceptionType.BAD_REQUEST, ex.getMessage());

        }


    }


}
