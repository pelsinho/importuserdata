package importing.user.data.exceptions;

public class UserImportDataValidationException extends Exception {

    public UserImportDataValidationException(String message) {
        super(message);
    }

}
