package importing.user.data.exceptions;

import java.util.Map;

public class ServiceException extends RuntimeException {
    private Map<String, String> errors;
    private ExceptionType type;
    private String message;

    public ServiceException(ExceptionType type, Map<String, String> errors, String message) {
        this.errors = errors;
        this.type = type;
        this.message = message;
    }

    public ServiceException(ExceptionType type, Map<String, String> errors) {
        this.errors = errors;
        this.type = type;
    }

    public ServiceException(ExceptionType type, String message) {
        this.type = type;
        this.message = message;
    }


    public Map<String, String> getErrors() {
        return errors;
    }

    public ExceptionType getType() {
        return type;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
