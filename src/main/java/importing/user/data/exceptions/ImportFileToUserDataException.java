package importing.user.data.exceptions;

public class ImportFileToUserDataException extends Exception {

    public ImportFileToUserDataException(String message) {
        super(message);
    }

}
