package importing.user.data.services.repositories;

import importing.user.data.data.entities.UserDataDB;
import importing.user.data.data.mappers.UserDataToUserDBMapper;
import importing.user.data.data.model.UserData;
import importing.user.data.repositories.UserDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserDataRepositoryServices {

    private final UserDataRepository userDataRepository;

    /**
     * Prepare and save data into Database
     */
    public List<UserDataDB> saveUserData(List<UserData> userDataList) {
        return userDataRepository.saveAll(getUserDataDB(userDataList));
    }

    private List<UserDataDB> getUserDataDB(List<UserData> userDataList) {
        return UserDataToUserDBMapper.userDataListToUserDBList(userDataList);
    }

}
