package importing.user.data.services.validations.manager;

import java.util.ArrayList;
import java.util.List;

public class ImportUserDataValidationHandlesMap implements IImportUserDataValidationHandlesMap<IImportUserDataValidationHandles> {

    private final ArrayList<IImportUserDataValidationHandles> handlesList;

    public ImportUserDataValidationHandlesMap() {

        handlesList = new ArrayList<>();
    }


    @Override
    public List<IImportUserDataValidationHandles> handlesList() {
        return handlesList;
    }

    @Override
    public void addHandle(IImportUserDataValidationHandles handle) {
        if (handle != null) {
            handlesList.add(handle);
        }
    }

}
