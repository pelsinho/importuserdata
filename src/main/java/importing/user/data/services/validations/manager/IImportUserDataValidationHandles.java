package importing.user.data.services.validations.manager;


public interface IImportUserDataValidationHandles<Q, P> {

    boolean canHandle(Q importUserDataValidationRequest);

    boolean isInvalidRequest(Q importUserDataValidationRequest);

    P addInvalidMessage(Q importUserDataValidationRequest);

}
