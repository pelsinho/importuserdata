package importing.user.data.services.validations.handles;

import importing.user.data.common.enums.ImportValidationPhase;
import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.services.validations.manager.IImportUserDataValidationHandles;
import org.springframework.stereotype.Component;

import java.util.Map;

import static importing.user.data.common.constants.ImportUserDataValidationMessages.ERROR_INVALID_IMPORT_FILE;


@Component
public class FileIsValidHandle implements IImportUserDataValidationHandles<ImportUserDataValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(ImportUserDataValidationRequest importUserDataValidationRequest) {
        return importUserDataValidationRequest.getImportValidationPhase().equals(ImportValidationPhase.FILE_VALIDATION);
    }

    /**
     * Validate if File Is Present
     */
    @Override
    public boolean isInvalidRequest(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return GenericPredicates.checkIfNullOrEmpty.test(importUserDataValidationRequest.getMultipartFile()) ||
                GenericPredicates.checkIfNullOrEmpty.test(importUserDataValidationRequest.getMultipartFile().getName());

    }

    @Override
    public Map<String, String> addInvalidMessage(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return Map.of(
                importUserDataValidationRequest.getImportValidationPhase().name(),
                ERROR_INVALID_IMPORT_FILE);

    }

}