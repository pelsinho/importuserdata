package importing.user.data.services.validations;

import importing.user.data.common.log.DefaultLog;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.data.model.ImportUserDataValidationResponse;
import importing.user.data.exceptions.UserImportDataValidationException;
import importing.user.data.services.validations.interfaces.IImportUserDataValidationProcess;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ImportUserDataValidationProcess implements IImportUserDataValidationProcess {

    private final ImportUserDataValidationControl importUserDataValidationControl;
    private final ImportUserDataValidationErrorServices importUserDataValidationErrorServices;

    /**
     * Controls ImportUserData Validation Process
     * Logs and Throws Exception if an invalid process has been founded
     */
    public void validateImportUserDataProcess(
            ImportUserDataValidationRequest importUserDataValidationRequest) throws UserImportDataValidationException {

        try {

            ImportUserDataValidationResponse importUserDataValidationResponse
                    = importUserDataValidationControl.isAValidImportUserDataRequest(importUserDataValidationRequest);
            importUserDataValidationErrorServices.checkValidationExistErrors(importUserDataValidationRequest, importUserDataValidationResponse);

        } catch (Exception ex) {

            DefaultLog.defaultLogError(ex);
            throw new UserImportDataValidationException(ex.getMessage());

        }

    }

}
