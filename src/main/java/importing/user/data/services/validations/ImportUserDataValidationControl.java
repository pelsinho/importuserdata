package importing.user.data.services.validations;

import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.data.model.ImportUserDataValidationResponse;
import importing.user.data.services.validations.handles.*;
import importing.user.data.services.validations.manager.IImportUserDataValidationHandles;
import importing.user.data.services.validations.manager.ImportUserDataValidationHandlesMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class ImportUserDataValidationControl {


    private final FileExtensionIsValidHandle fileExtensionIsValidHandle;
    private final FileSizeIsValidHandle fileSizeIsValidHandle;
    private final FileIsValidHandle fileIsValidHandle;
    private final UserDataHasDuplicateEmailsHandle userDataHasDuplicateEmailsHandle;
    private final UserDataFieldsAreValidHandle userDataFieldsAreValidHandle;
    private final UserDataListIsValidHandle userDataListIsValidHandle;


    private ImportUserDataValidationHandlesMap importUserDataValidationHandlesMap;

    @PostConstruct
    private void startImportUserDataValidationHandles() {
        populateImportUserDataValidationHandles();
    }

    private void populateImportUserDataValidationHandles() {

        //ImportUserData Validation Handles
        importUserDataValidationHandlesMap = new ImportUserDataValidationHandlesMap();
        importUserDataValidationHandlesMap.addHandle(fileExtensionIsValidHandle);
        importUserDataValidationHandlesMap.addHandle(fileSizeIsValidHandle);
        importUserDataValidationHandlesMap.addHandle(fileIsValidHandle);
        importUserDataValidationHandlesMap.addHandle(userDataHasDuplicateEmailsHandle);
        importUserDataValidationHandlesMap.addHandle(userDataFieldsAreValidHandle);
        importUserDataValidationHandlesMap.addHandle(userDataListIsValidHandle);

    }


    /**
     * Controls Validation Handles
     * Add Invalid Message if necessary
     */
    protected ImportUserDataValidationResponse isAValidImportUserDataRequest(
            ImportUserDataValidationRequest importUserDataValidationRequest) {

        ImportUserDataValidationResponse importUserDataValidationResponse = new ImportUserDataValidationResponse();

        for (IImportUserDataValidationHandles importUserDataValidationHandles : importUserDataValidationHandlesMap.handlesList()) {

            if (importUserDataValidationHandles.canHandle(importUserDataValidationRequest) && importUserDataValidationHandles.isInvalidRequest(importUserDataValidationRequest)) {

                /**
                * I set to validate all the possible errors in the request
                * If we want we can add a break and add just the first error founded.
                */

                importUserDataValidationResponse.getInvalidMessages().putAll((Map<String, String>) importUserDataValidationHandles.addInvalidMessage(importUserDataValidationRequest));

            }

        }

        return importUserDataValidationResponse;

    }

}
