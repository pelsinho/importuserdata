package importing.user.data.services.validations.handles;

import importing.user.data.common.enums.ImportValidationPhase;
import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.common.validations.BindingResultValidation;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.services.validations.manager.IImportUserDataValidationHandles;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

import static importing.user.data.common.constants.ImportUserDataValidationMessages.ERROR_INVALID_USER_DATA_OBJECT;

@RequiredArgsConstructor
@Component
public class UserDataFieldsAreValidHandle implements IImportUserDataValidationHandles<ImportUserDataValidationRequest, Map<String, String>> {

    private final BindingResultValidation bindingResultValidation;

    @Override
    public boolean canHandle(ImportUserDataValidationRequest importUserDataValidationRequest) {
        return importUserDataValidationRequest.getImportValidationPhase().equals(ImportValidationPhase.USER_DATA_VALIDATION) &&
                GenericPredicates.checkIfNullOrEmpty.negate().test(importUserDataValidationRequest.getUserDataList());
    }

    /**
     * Validate if UserData Fields are valid, based on Spring Annotations
     */
    @Override
    public boolean isInvalidRequest(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return importUserDataValidationRequest.getUserDataList()
                .parallelStream()
                .anyMatch(user -> bindingResultValidation.validateBindingResultErrorsFromDto(user).isPresent());

    }

    @Override
    public Map<String, String> addInvalidMessage(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return Map.of(
                importUserDataValidationRequest.getImportValidationPhase().name(),
                ERROR_INVALID_USER_DATA_OBJECT);

    }

}