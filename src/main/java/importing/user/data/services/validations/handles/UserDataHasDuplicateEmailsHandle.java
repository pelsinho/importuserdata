package importing.user.data.services.validations.handles;

import importing.user.data.common.enums.ImportValidationPhase;
import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.common.predicates.UserDataPredicates;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.services.validations.manager.IImportUserDataValidationHandles;
import org.springframework.stereotype.Component;

import java.util.Map;

import static importing.user.data.common.constants.ImportUserDataValidationMessages.ERROR_INVALID_USER_EMAIL_DATA;


@Component
public class UserDataHasDuplicateEmailsHandle implements IImportUserDataValidationHandles<ImportUserDataValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(ImportUserDataValidationRequest importUserDataValidationRequest) {
        return importUserDataValidationRequest.getImportValidationPhase().equals(ImportValidationPhase.USER_DATA_VALIDATION) &&
                GenericPredicates.checkIfNullOrEmpty.negate().test(importUserDataValidationRequest.getUserDataList());
    }

    /**
     * Validate if UserData List has duplicated e-mails
     */
    @Override
    public boolean isInvalidRequest(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return UserDataPredicates.thereIsNoDuplicateUserEmail.negate().test(importUserDataValidationRequest.getUserDataList());

    }

    @Override
    public Map<String, String> addInvalidMessage(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return Map.of(
                importUserDataValidationRequest.getImportValidationPhase().name(),
                ERROR_INVALID_USER_EMAIL_DATA);

    }

}