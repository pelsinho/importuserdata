package importing.user.data.services.validations.handles;

import importing.user.data.common.enums.ImportValidationPhase;
import importing.user.data.common.predicates.FilePredicates;
import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.services.validations.manager.IImportUserDataValidationHandles;
import org.springframework.stereotype.Component;

import java.util.Map;

import static importing.user.data.common.constants.ImportUserDataValidationMessages.ERROR_INVALID_FILE_SIZE;


@Component
public class FileSizeIsValidHandle implements IImportUserDataValidationHandles<ImportUserDataValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(ImportUserDataValidationRequest importUserDataValidationRequest) {
        return importUserDataValidationRequest.getImportValidationPhase().equals(ImportValidationPhase.FILE_VALIDATION) &&
                GenericPredicates.checkIfNullOrEmpty.negate().test(importUserDataValidationRequest.getMultipartFile());
    }

    /**
     * Validate if File Has a Invalid Size
     */
    @Override
    public boolean isInvalidRequest(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return FilePredicates.isFileHasAInvalidSize.test(importUserDataValidationRequest.getMultipartFile());

    }

    @Override
    public Map<String, String> addInvalidMessage(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return Map.of(
                importUserDataValidationRequest.getImportValidationPhase().name(),
                ERROR_INVALID_FILE_SIZE);

    }

}