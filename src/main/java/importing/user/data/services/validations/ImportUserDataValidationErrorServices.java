package importing.user.data.services.validations;

import importing.user.data.common.functions.GenericFunctions;
import importing.user.data.common.log.DefaultLog;
import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.data.model.ImportUserDataValidationResponse;
import importing.user.data.exceptions.UserImportDataValidationException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ImportUserDataValidationErrorServices {

    protected void checkValidationExistErrors(
            ImportUserDataValidationRequest importUserDataValidationRequest,
            ImportUserDataValidationResponse importUserDataValidationResponse) throws UserImportDataValidationException {

        if (GenericPredicates.checkIfNullOrEmpty.negate().test(importUserDataValidationResponse.getInvalidMessages())) {

            String errorMsg = GenericFunctions.getMapValueAsStringBy.apply(importUserDataValidationResponse.getInvalidMessages(), ",");

            DefaultLog.validationLogError(importUserDataValidationRequest.getImportValidationPhase(), errorMsg);

            throw new UserImportDataValidationException(errorMsg);

        }

    }

}
