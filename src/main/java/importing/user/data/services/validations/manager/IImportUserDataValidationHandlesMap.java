package importing.user.data.services.validations.manager;

import java.util.List;

public interface IImportUserDataValidationHandlesMap<H> {

    List<H> handlesList();

    void addHandle(H handle);

}
