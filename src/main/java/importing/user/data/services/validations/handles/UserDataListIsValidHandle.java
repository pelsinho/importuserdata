package importing.user.data.services.validations.handles;

import importing.user.data.common.enums.ImportValidationPhase;
import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.services.validations.manager.IImportUserDataValidationHandles;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

import static importing.user.data.common.constants.ImportUserDataValidationMessages.ERROR_INVALID_USER_DATA_EMPTY;

@RequiredArgsConstructor
@Component
public class UserDataListIsValidHandle implements IImportUserDataValidationHandles<ImportUserDataValidationRequest, Map<String, String>> {

    @Override
    public boolean canHandle(ImportUserDataValidationRequest importUserDataValidationRequest) {
        return importUserDataValidationRequest.getImportValidationPhase().equals(ImportValidationPhase.USER_DATA_VALIDATION);
    }

    /**
     * Validate if User Data List is not empty, or not null
     */
    @Override
    public boolean isInvalidRequest(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return GenericPredicates.checkIfNullOrEmpty.test(importUserDataValidationRequest.getUserDataList());

    }

    @Override
    public Map<String, String> addInvalidMessage(ImportUserDataValidationRequest importUserDataValidationRequest) {

        return Map.of(
                importUserDataValidationRequest.getImportValidationPhase().name(),
                ERROR_INVALID_USER_DATA_EMPTY);

    }

}