package importing.user.data.services;

import importing.user.data.common.enums.ImportValidationPhase;
import importing.user.data.data.dto.UserDataDTO;
import importing.user.data.data.entities.UserDataDB;
import importing.user.data.data.mappers.ImportFileToUserDataListMapper;
import importing.user.data.data.mappers.UserDataDBToUserDataDTOMapper;
import importing.user.data.data.model.ImportUserDataValidationRequest;
import importing.user.data.data.model.UserData;
import importing.user.data.exceptions.ImportFileToUserDataException;
import importing.user.data.exceptions.UserImportDataValidationException;
import importing.user.data.services.interfaces.IImportUserDataService;
import importing.user.data.services.repositories.UserDataRepositoryServices;
import importing.user.data.services.validations.ImportUserDataValidationProcess;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ImportUserDataService implements IImportUserDataService {

    private final ImportUserDataValidationProcess importUserDataValidationProcess;
    private final UserDataRepositoryServices userDataRepositoryServices;

    public List<UserDataDTO> importUserDataService(MultipartFile multipartFile) throws ImportFileToUserDataException, UserImportDataValidationException {

        /**
         * Validate File Process
         * Use Chain of Responsibility to avoid multiple if´s, and de-couple handles validation
         * Throws Exception if there is any errors
         */

        importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest.builder()
                    .multipartFile(multipartFile)
                    .importValidationPhase(ImportValidationPhase.FILE_VALIDATION)
                    .build());


        /**
         * Transform File to User Data Object
         */

        List<UserData> userDataList = ImportFileToUserDataListMapper.importFileToUserDataList(multipartFile);


        /**
         * Validate User Data Object
         * Use Chain of Responsibility to avoid multiple if´s, and de-couple handles validation
         * Throws Exception if there is any errors
         */

        importUserDataValidationProcess.validateImportUserDataProcess(
                ImportUserDataValidationRequest.builder()
                        .userDataList(userDataList)
                        .importValidationPhase(ImportValidationPhase.USER_DATA_VALIDATION)
                        .build());


        /**
         * Call Repository Services to Run Save/Update Process
         */

        List<UserDataDB> userDataDBList = userDataRepositoryServices.saveUserData(userDataList);


        /**
         * Transform UserDataDB to User Data Object And Return It
         */

        return UserDataDBToUserDataDTOMapper.userDataListToUserDTOList(userDataDBList);


    }


}
