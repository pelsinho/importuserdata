package importing.user.data.data.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_data")
public class UserDataDB extends Audit {

    @Id
    @Column(unique = true)
    @NotNull
    @NotEmpty
    private String email;

    private String lastName;

    private String firstName;

    private String fiscalCode;

    private String description;

    private LocalDate lastAccessDate;

}