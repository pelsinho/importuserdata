package importing.user.data.data.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDataDTO {

    private String email;

    private String lastName;

    private String firstName;

    private String fiscalCode;

    private String description;

    private LocalDate lastAccessDate;

}