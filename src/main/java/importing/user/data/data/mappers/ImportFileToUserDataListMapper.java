package importing.user.data.data.mappers;

import com.opencsv.bean.CsvToBeanBuilder;
import importing.user.data.common.log.DefaultLog;
import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.data.model.UserData;
import importing.user.data.exceptions.ImportFileToUserDataException;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class ImportFileToUserDataListMapper {

    private ImportFileToUserDataListMapper() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static List<UserData> importFileToUserDataList(@NotEmpty @NotNull MultipartFile multipartFile) throws ImportFileToUserDataException {

        try {

            if (GenericPredicates.checkIfNullOrEmpty.test(multipartFile) ||
                    GenericPredicates.checkIfNullOrEmpty.test(multipartFile.getInputStream())) {
                return new ArrayList<>();
            }

            Reader reader = new InputStreamReader(multipartFile.getInputStream());
            return new CsvToBeanBuilder(reader)
                    .withType(UserData.class)
                    .build()
                    .parse();

        } catch (Exception ex) {

            DefaultLog.defaultLogError(ex);
            throw new ImportFileToUserDataException(ex.getMessage());

        }

    }

}
