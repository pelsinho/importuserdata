package importing.user.data.data.mappers;

import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.data.dto.UserDataDTO;
import importing.user.data.data.entities.UserDataDB;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDataDBToUserDataDTOMapper {

    private UserDataDBToUserDataDTOMapper() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static List<UserDataDTO> userDataListToUserDTOList(List<UserDataDB> userDataDBList) {

        if (GenericPredicates.checkIfNullOrEmpty.test(userDataDBList)) return new ArrayList<>();

        return userDataDBList
                .parallelStream()
                .map(userData ->
                        UserDataDTO.builder()
                            .description(userData.getDescription())
                            .email(userData.getEmail())
                            .firstName(userData.getFirstName())
                            .lastName(userData.getLastName())
                            .lastAccessDate(userData.getLastAccessDate())
                            .fiscalCode(userData.getFiscalCode())
                            .build())
                .collect(Collectors.toList());

    }


}
