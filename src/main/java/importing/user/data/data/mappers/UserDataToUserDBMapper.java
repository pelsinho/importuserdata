package importing.user.data.data.mappers;

import importing.user.data.common.functions.GenericFunctions;
import importing.user.data.common.predicates.GenericPredicates;
import importing.user.data.data.entities.UserDataDB;
import importing.user.data.data.model.UserData;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDataToUserDBMapper {

    private UserDataToUserDBMapper() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static List<UserDataDB> userDataListToUserDBList(List<UserData> userDataList) {

        if (GenericPredicates.checkIfNullOrEmpty.test(userDataList)) return new ArrayList<>();

        return userDataList
                .parallelStream()
                .map(userData ->
                        UserDataDB.builder()
                            .description(userData.getDescription())
                            .email(userData.getEmail())
                            .firstName(userData.getFirstName())
                            .lastName(userData.getLastName())
                            .lastAccessDate(getLocalDate(userData))
                            .fiscalCode(userData.getFiscalCode())
                            .build())
                .collect(Collectors.toList());

    }

    private static LocalDate getLocalDate(UserData userData) {

        if (GenericPredicates.checkIfNullOrEmpty.test(userData.getLastAccessDate())) {
            return null;
        }

        return GenericFunctions.getLocalDateFormatter.apply(userData.getLastAccessDate());

    }

}
