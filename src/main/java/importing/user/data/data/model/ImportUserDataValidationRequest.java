package importing.user.data.data.model;

import importing.user.data.common.enums.ImportValidationPhase;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * This is a Generic Request Object for Validation Process, in the future we may add more validation objects
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImportUserDataValidationRequest {

    private List<UserData> userDataList;

    private MultipartFile multipartFile;

    @NotNull
    @NotEmpty
    private ImportValidationPhase importValidationPhase;

}
