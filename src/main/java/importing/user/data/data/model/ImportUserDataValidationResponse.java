package importing.user.data.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a Generic Response Object for Validation Process, in the future we may add more validation objects
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImportUserDataValidationResponse {

    private Map<String, String> invalidMessages = new HashMap<>();

}
