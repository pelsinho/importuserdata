package importing.user.data.data.model;


import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static importing.user.data.common.constants.ImportUserDataInformation.*;
import static importing.user.data.common.constants.ImportUserDataValidationMessages.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserData {

    @CsvBindByPosition(position = 0)
    @Pattern(regexp = EMAIL_VERIFICATION_PATTERN, message = ERROR_INVALID_EMAIL_PATTERN)
    @NotNull
    @NotEmpty
    private String email;

    @CsvBindByPosition(position = 1)
    @Size(max = 100)
    private String lastName;

    @CsvBindByPosition(position = 2)
    @Size(max = 100)
    private String firstName;

    @CsvBindByPosition(position = 3)
    @Pattern(regexp = FISCAL_CODE_PATTERN, message = ERROR_INVALID_FISCAL_CODE_PATTERN)
    private String fiscalCode;

    @CsvBindByPosition(position = 4)
    @Size(max = 100)
    private String description;

    @CsvBindByPosition(position = 5)
    @Pattern(regexp = LAST_ACCESS_DATE_PATTERN, message = ERROR_INVALID_LAST_ACCESS_DATE_PATTERN)
    private String lastAccessDate;

}