package importing.user.data.common.log;

import importing.user.data.common.enums.ImportValidationPhase;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultLog {

    private DefaultLog() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Default Method to have a common log error msg.
     * Can be add more information in the future if necessary
     * Like Objects information
     */
    public static void defaultLogError(Exception ex) {

        log.error(String.format("ERROR - on Class : %s - on Method : %s " +
                        " - StackTrace : ",
                ex.getStackTrace()[0].getClassName(),
                ex.getStackTrace()[0].getMethodName()), ex);

    }



    public static void validationLogError(
            ImportValidationPhase importValidationPhase,
            String errorMsg) {

        log.error(String.format("ERROR - on Validation Phase : %s - Error Messages : %s ",
                importValidationPhase,
                errorMsg));

    }


}
