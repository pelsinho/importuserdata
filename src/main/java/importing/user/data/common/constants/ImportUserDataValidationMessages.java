package importing.user.data.common.constants;


import static importing.user.data.common.constants.ImportUserDataInformation.*;

public class ImportUserDataValidationMessages {

    private ImportUserDataValidationMessages() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static final String ERROR_INVALID_FILE_SIZE =
            "Invalid - File Size - Maximum File Size is : " + FILE_LIMIT_MAX_SIZE_IN_KB + "kb";

    public static final String ERROR_INVALID_FILE_EXTENSION =
            "Invalid - File Extension - Please File Extension must be : " + FILE_EXTENSION;

    public static final String ERROR_INVALID_USER_EMAIL_DATA =
            "Invalid - User Email Data - There is more than user with same e-mail in the file. Please validate it";

    public static final String ERROR_INVALID_USER_DATA_OBJECT = "Invalid - User Data Object";

    public static final String ERROR_INVALID_USER_DATA_EMPTY = "Invalid - User Import Data is Empty";

    public static final String ERROR_INVALID_IMPORT_FILE = "Invalid - User Import File is Empty or Null";

    public static final String ERROR_INVALID_EMAIL_PATTERN = "Invalid email, please use the following regex to validate it : " + EMAIL_VERIFICATION_PATTERN;

    public static final String ERROR_INVALID_LAST_ACCESS_DATE_PATTERN = "Invalid last access date, please use the following regex to validate it : " + LAST_ACCESS_DATE_PATTERN;

    public static final String ERROR_INVALID_FISCAL_CODE_PATTERN = "Invalid fiscal code, please use the following regex to validate it : " + FISCAL_CODE_PATTERN;



}
