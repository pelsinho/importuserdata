package importing.user.data.common.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ImportUserDataInformation {

    /**
     * DATE FORMAT
     */
    public static final String LAST_ACCESS_DATE_FORMAT = "dd-MM-yyyy";

    /**
     * FILE INFO
     */
    public static final String FILE_EXTENSION = ".csv";
    public static final Integer FILE_MIN_SIZE = FILE_EXTENSION.length();

    /**
     * REGEX PATTERNS
     */

    public static final String EMAIL_VERIFICATION_PATTERN = "^[\\w-_\\.+]*[\\w-_\\.]\\@([a-zA-Z0-9_-]+\\.)+[\\w]+[\\w]$";
    public static final String FISCAL_CODE_PATTERN = "^$|^[a-zA-Z0-9-._]{1,100}$";
    public static final String LAST_ACCESS_DATE_PATTERN
            = "^$|^(0[1-9]|1[0-9]|2[0-9]|3[0-1])+(-)+(0[1-9]|1[0-2])+(-)+(20[0-9][0-9])$";


    /**
     * FROM ENV VARIABLES
     * To be able to change the size
     */

    public static Integer FILE_LIMIT_MAX_SIZE_IN_KB;

    @Value("${file.limit.size.in.kb}")
    public void setFileLimitSize(Integer fileLimitSizeInKb) {
        FILE_LIMIT_MAX_SIZE_IN_KB = fileLimitSizeInKb;
    }



}
