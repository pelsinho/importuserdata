package importing.user.data.common.predicates;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.function.Predicate;

import static importing.user.data.common.constants.ImportUserDataInformation.*;

public class FilePredicates {


    private FilePredicates() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Validate If file is bigger than file size limit
     */
    public static Predicate<@NotNull MultipartFile> isFileHasAInvalidSize = file ->
            FILE_LIMIT_MAX_SIZE_IN_KB <= file.getSize();

    /**
     * Validate If file has the correct file_extension
     */
    public static Predicate<@NotNull MultipartFile> isFileHasAInvalidExtension = file ->
            GenericPredicates.checkIfNullOrEmpty.test(file) ||
            GenericPredicates.checkIfNullOrEmpty.test(file.getOriginalFilename()) ||
            file.getOriginalFilename().length() < FILE_MIN_SIZE ||
            !file.getOriginalFilename().endsWith(FILE_EXTENSION);

}
