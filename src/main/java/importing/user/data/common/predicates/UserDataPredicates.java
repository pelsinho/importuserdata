package importing.user.data.common.predicates;

import importing.user.data.data.model.UserData;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.function.Predicate;

public class UserDataPredicates {

    private UserDataPredicates() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Validate if there are duplicate user e-mails
     */
    public static Predicate<@NotNull List<UserData>> thereIsNoDuplicateUserEmail = userDataList ->
            GenericPredicates.checkIfNullOrEmpty.test(userDataList) ||
            userDataList.stream().allMatch(new HashSet<>()::add);


}
