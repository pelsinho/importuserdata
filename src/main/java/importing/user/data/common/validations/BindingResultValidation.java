package importing.user.data.common.validations;

import importing.user.data.common.functions.GenericFunctions;
import importing.user.data.data.dto.ResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.badRequest;

@Component
@RequiredArgsConstructor
@Slf4j
public class BindingResultValidation {

    private final LocalValidatorFactoryBean localValidatorFactoryBean;

    /**
     * Validate Spring Binding Result Process
     * If there is error(s) return a BadRequest with message(s) error(s) in the body
     */
    public ResponseEntity<ResponseDTO> validateBindingResult(BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {

            ResponseDTO responseDTO = new ResponseDTO();
            responseDTO.setDetails(getBindingResultErrorsMap(bindingResult));

            return badRequest().body(responseDTO);

        }

        return null;
    }


    /**
     * Validate Spring Binding Result Process
     * If there is error(s) return a msg will be returned with error(s)
     */
    public Optional<String> validateBindingResultErrorsFromDto(Object object) {

        DataBinder binder = new DataBinder(object);
        binder.addValidators(localValidatorFactoryBean);
        binder.validate();

        if (binder.getBindingResult().hasErrors()) {

            Map<String, String> bindingResultErrorsMap = getBindingResultErrorsMap(binder.getBindingResult());
            return Optional.of(GenericFunctions.getMapValueAsStringBy.apply(bindingResultErrorsMap, null));

        }

        return Optional.empty();
    }


    private Map<String, String> getBindingResultErrorsMap(BindingResult result) {

        Map<String, String> map = new HashMap<>();
        for (ObjectError error : result.getAllErrors()) {
            if (error instanceof FieldError) {
                FieldError fieldError = (FieldError) error;
                map.put(fieldError.getField(), fieldError.getDefaultMessage());
            } else {
                map.put(error.getCode(), error.getDefaultMessage());
            }
        }
        return map;
    }

}
