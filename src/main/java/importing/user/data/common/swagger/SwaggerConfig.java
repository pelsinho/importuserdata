package importing.user.data.common.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

import static springfox.documentation.builders.RequestHandlerSelectors.withClassAnnotation;

/**
 * Swagger Control Class
 */
@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .paths(PathSelectors.any())
                .apis(withClassAnnotation(RestController.class))
                .build()
                .apiInfo(internalApiInfo())
                .globalResponseMessage(RequestMethod.GET, responseMessage())
                .globalResponseMessage(RequestMethod.POST, responseMessage())
                .globalResponseMessage(RequestMethod.PUT, responseMessage())
                .globalResponseMessage(RequestMethod.DELETE, responseMessage());
    }



    private ApiInfo internalApiInfo() {
        String title = "Import User Data API";
        String description =
                        "<p>Import User Data Documentation</p>" +
                        "<p>The file can contain an arbitrary number of rows. Each row contains such fields in the following order: email, Last Name, First Name, Fiscal Code, Description, Last Access Date</p>" +
                        "<p>Don´t add free spaces between csv fields. May lead to unexpected problems</p>" +
                        "<p>You can change maximum size file in application.properties, file.limit.size.in.kb property</p>" +
                        "<p>Notes : </p>" +
                        "<p>1 - There cannot be more users with the same email in the DB</p>" +
                        "<p>2 - All fields (with the exception of email) can be null</p>" +
                        "<p>3 - Any file is the new master Last Access Date</p>" +
                        "<p>Scenarios : </p>" +
                        "<p>1 - Given a new file to import, when I have imported the file, then the db state has to be updated</p>" +
                        "<p>2 - Given an existing user, when the new imported file contains the same email, then the existing user data has to be updated</p>" +
                        "<p>3 - Given a new file to import, when I upload a *.txt file, then I should receive an errors</p>" +
                        "<p>4 - Given a new file to import, when I upload a file that exceeds the limit (in MB), then I should receive an error</p>" +
                        "<p>Expectations : </p>" +
                        "<p>1 - Modelling needed entities</p>" +
                        "<p>2 - Handling errors</p>" +
                        "<p>3 - Saving the file everywhere you prefer (Keep in mind the storage provider could change)</p>" +
                        "<p>4 - Highlighting complexities you have to cover</p>" +
                        "<p>5 - Being able to import a file containing 10k rows</p>" +
                        "<p>6 - Writing tests!</p>" +
                        "<p>If you need to register a new user please contact our support team.</p>" +
                        "<p>You will find more information in each Dto/Endpoint documentation.</p>";
        String version = "0.0.1";
        Contact contact = new Contact("MotorK Support Team", "https://www.motork.io/contacts/", "motork@support.com");

        return new ApiInfo(title, description, version, null, contact, null, null,
                Collections.emptyList());
    }


    private List<ResponseMessage> responseMessage() {

        ResponseMessage internalServerError = new ResponseMessageBuilder()
                .code(500)
                .message("Internal server error")
                .build();

        ResponseMessage forbidden = new ResponseMessageBuilder()
                .code(403)
                .message("Forbidden")
                .build();

        ResponseMessage unauthorised = new ResponseMessageBuilder()
                .code(401)
                .message("Unauthorised")
                .build();

        ResponseMessage notFound = new ResponseMessageBuilder()
                .code(404)
                .message("Not found")
                .build();

        ResponseMessage success = new ResponseMessageBuilder()
                .code(200)
                .message("Success")
                .build();

        ResponseMessage invalidRequestBody = new ResponseMessageBuilder()
                .code(422)
                .message("Invalid request body")
                .build();

        return List.of(
                internalServerError,
                forbidden,
                unauthorised,
                notFound,
                success,
                invalidRequestBody);

    }
}
