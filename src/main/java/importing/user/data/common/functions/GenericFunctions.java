package importing.user.data.common.functions;

import importing.user.data.common.predicates.GenericPredicates;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static importing.user.data.common.constants.ImportUserDataInformation.LAST_ACCESS_DATE_FORMAT;

public class GenericFunctions {

    private GenericFunctions() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Format a Map Values to a String splitted by
     */
    public static BiFunction<@NotNull @NotEmpty Map<String, ?>, String, String> getMapValueAsStringBy = (map, splitBy) -> {

        if (GenericPredicates.checkIfNullOrEmpty.test(map)) return "";

        splitBy = GenericPredicates.checkIfNullOrEmpty.test(splitBy) ? "," : splitBy;

        return map.keySet().stream()
                .map(key -> map.get(key) + " ")
                .collect(Collectors.joining(splitBy, "", ""));

    };


    /**
     * Transform a String into a Local Date using application pattern format
     */
    public static Function<@NotNull @NotEmpty String, LocalDate> getLocalDateFormatter = localDate -> {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(LAST_ACCESS_DATE_FORMAT);
        return LocalDate.parse(localDate, dateTimeFormatter);

    };



}
