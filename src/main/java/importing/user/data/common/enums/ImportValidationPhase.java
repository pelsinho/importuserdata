package importing.user.data.common.enums;

/**
 * Validation Process Phases. Control Handles Process
 */
public enum ImportValidationPhase {

    FILE_VALIDATION,
    USER_DATA_VALIDATION;

}
