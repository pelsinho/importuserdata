package importing.user.data.repositories;

import importing.user.data.data.entities.UserDataDB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDataRepository extends JpaRepository<UserDataDB, Long> {

    List<UserDataDB> findByEmailIn(List<String> emailList);

}
